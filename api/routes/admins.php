<?php
	error_reporting(1);
	
	//prevent CORS error
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	
	//including the required files
	include_once("vendor/autoload.php");

	// Main SLIM Framework PSR-7 Functions
	use \Psr\Http\Message\ServerRequestInterface as Request;
	use \Psr\Http\Message\ResponseInterface as Response;

	$app = new \Slim\App; //init slimphp
	
	//vars 
	$created_at = date("Y-m-d H:i:s");
	
	//remove lead notes
	$app->post('/deleteleadnotes', function ($request, $response, $args) {
		$allPostPutVars = $request->getParsedBody();
		$payload["message"] = "";
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}

		if($db->query("DELETE FROM lead_notes WHERE id = '".$allPostPutVars['lead_note_id']."' ")){
			if($db->affected_rows) {
				$payload["message"] = "Success";
			}
		}
		
		// Return Response
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		// Return Response
		return $response;
	});
	
	//get lead notes
	$app->post('/getleadnotes', function ($request, $response, $args) {
		$resultsToArray = array();
		$allPostPutVars = $request->getParsedBody();
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database ['.$db->connect_error.']');
		}

		if($result = $db->query("SELECT id, note_title, note_body, created_by, created_user_id, created_at FROM lead_notes WHERE created_user_id = '".$allPostPutVars['login_user_id']."'")) {
			while($row = $result->fetch_assoc()){
				array_push($resultsToArray, $row);
			}
		}
		
		$payload["lead_notes"] = $resultsToArray;
		
		// Return Response
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		// Return Response
		return $response;
	});

	//create lead notes
	$app->post('/createleadnotes', function ($request, $response, $args) {
		$allPostPutVars = $request->getParsedBody();
		$last_inserted_id = "";
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}

		if($db->query("INSERT INTO lead_notes (note_title, note_body, created_by, created_user_id, created_at) VALUES('".mysql_real_escape_string($allPostPutVars['note_title'])."', '".mysql_real_escape_string($allPostPutVars['note_body'])."', '".$allPostPutVars['created_by']."', '".$allPostPutVars['created_id']."', '".date("Y-m-d H:i:s A")."')")){
			$last_inserted_id = $db->insert_id;
		}
		if($db->affected_rows) {
			$payload["message"] = "Success";
			$payload["ret_id"] = $last_inserted_id;
		}
		
		// Return Response
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		// Return Response
		return $response;
	});
	
	//get lead
	$app->post('/getlead', function ($request, $response, $args) {
		$resultsToArray = array();
		$allPostPutVars = $request->getParsedBody();
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database ['.$db->connect_error.']');
		}

		if($result = $db->query("SELECT lead_name, lead_company, lead_title, lead_email, lead_phone, lead_mobile, lead_website, lead_address, table_id, created_at FROM leads WHERE id = '".$allPostPutVars['lead_id']."'")) {
			while($row = $result->fetch_assoc()){
				array_push($resultsToArray, $row);
			}
		}
		
		$payload["lead_detail"] = $resultsToArray;
		
		// Return Response
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		// Return Response
		return $response;
	});
	
	//create lead
	$app->post('/createlead', function ($request, $response, $args) {
		$allPostPutVars = $request->getParsedBody();
		$last_inserted_id = "";
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}

		if($db->query("INSERT INTO leads (lead_name, lead_company, lead_title, lead_email, lead_phone, lead_mobile, lead_website, lead_address, table_id, created_at)
						VALUES('".$allPostPutVars['lead_name']."', '".$allPostPutVars['lead_company']."', '".$allPostPutVars['lead_title']."', 
								'".$allPostPutVars['lead_email']."', '".$allPostPutVars['lead_phone']."', '".$allPostPutVars['lead_mobile']."',
								'".$allPostPutVars['lead_website']."', '".$allPostPutVars['lead_address']."', '".$allPostPutVars['table_id']."', '".$created_at."')")){
			$last_inserted_id = $db->insert_id;
		}
		if($db->affected_rows) {
			$payload["message"] = "Success";
			$payload["ret_id"] = $last_inserted_id;
		}
		
		// Return Response
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		// Return Response
		return $response;
	});
	
	//update table ordinal position
	$app->post('/updateOrdinalPosition', function ($request, $response, $args) {
		$allPostPutVars = $request->getParsedBody();
		$update_count = 0;
		$ordinal_message = "";
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0) {
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
	
		$dragged_element = $db->query("UPDATE tables SET table_ordinal_position = '".$allPostPutVars['dragged_element_position']."' WHERE ID = '".$allPostPutVars['dragged_element_id']."' ");
		if($db->affected_rows) {
			$previous_dropped = $db->query("UPDATE tables SET table_ordinal_position = '".$allPostPutVars['previous_dropped_position']."' WHERE ID = '".$allPostPutVars['previous_dropped_id']."' ");
			if($db->affected_rows) {
				$ordinal_message = " Tables ordinal moved successfully";
			}
		}
		
		// Return Response
		$payload["message"] = "Success";
		$payload["tables"] = $allPostPutVars;
		$payload["table_ordinal"] = $ordinal_message;
		
		// status
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		
		// Return Response
		return $response;
	});
	
	//create table
	$app->post('/createtable', function ($request, $response, $args) {
		$allPostPutVars = $request->getParsedBody();
		$resultsToArray = array();
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		
		//get table top ordinal position
		if(!$top_ordinal = $db->query("SELECT MAX(ID) FROM tables")){
			die('There was an error running the query [' . $db->error . ']');
		}
		$top_ordinal_row = $top_ordinal->fetch_row();
		$top_ordinal_row[0] = $top_ordinal_row[0] + 1;
		$last_inserted_id = "";
		if($db->query("INSERT INTO tables (table_name, table_color, table_ordinal_position)VALUES('".$allPostPutVars['tablename']."', '".$allPostPutVars['tablebgcolor']."', '".$top_ordinal_row[0]."')")){
			$last_inserted_id = $db->insert_id;
			if(!$result = $db->query("SELECT * FROM tables")){
				die('There was an error running the query [' . $db->error . ']');
			}
			while($row = $result->fetch_assoc()){
				array_push($resultsToArray, $row);
			}
		}
		
		// Return Response
		$payload["message"] = "Success";
		$payload["tables"] = count($resultsToArray);
		$payload["top_ordinal"] =  $top_ordinal_row[0];
		$payload["ret_id"] =  $last_inserted_id;
		
		// status
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		
		// Return Response
		return $response;
	});
	
	//get tables
	$app->get('/gettables', function ($request, $response, $args) {
		$resultsToArray = array();
		
		$db = new mysqli("localhost", "rmpl", "p@55w0rd", "rumple_leads");
		if($db->connect_errno > 0){
			die('Unable to connect to database [' . $db->connect_error . ']');
		}
		if(!$result = $db->query("SELECT * FROM tables ORDER BY table_ordinal_position ASC")){
			die('There was an error running the query ['. $db->error.']');
		}

		while($row = $result->fetch_assoc()){
			$count = 0;
			$lead_result = $db->query("SELECT id, lead_name, lead_company, created_at FROM leads WHERE table_id = '".$row['id']."' ORDER BY id DESC");
			
			$leads = array();
			while($lead_row = $lead_result->fetch_assoc()){
				array_push($leads, $lead_row);
				$row = array_merge($row, array("leads"=> $leads));
			}
			
			array_push($resultsToArray, $row);
			
			$count++;
		}
		
		// Return Response
		$payload["message"] = "Success";
		$payload["count"] = count($resultsToArray);
		$payload["tables"] = $resultsToArray;
		
		// status
		$status = 200;
		$status_message = "Success";
	
		// Set the response
		$response = $response->withJson($payload);
		$response = $response->withStatus($status, $status_message);
		
		
		// Return Response
		return $response;
	});
	
	
	$app->run();




?>