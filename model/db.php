<?php
	class DB {
		//Constructor
		function __contruct() {
			$this->dbConnect();
		}
		//connection to mysql database
		public function dbConnect() {
			global $dbConn, $siteState;	
			switch($siteState) {
				case "Online":
					$DBhost = "localhost";
					$DBName = "rumple_leads";
					$DBuser = "rumple_user";
					$DBpass = "p@55w0rd";
				case "Local":
					$DBhost = "localhost";
					$DBName = "rumple_leads";
					$DBuser = "rumple_user";
					$DBpass = "p@55w0rd";
			}
			$dbConn = new PDO("mysql:host=$DBhost;dbname=$DBName", $DBuser, $DBpass);
			$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			if ($dbConn) {
				mysql_select_db($DBName, $dbConn);
				return true;
			}
			return false;
		}
		//Close Database connection
		public function closeDb($clsdb) {
			mysql_close($clsdb);
		}
		//build SQL Statement
		public function buildSQL($obj) {
			switch ($obj['op']) {
				case "SELECT":
					if ($obj['op'] == "" || $obj['tables'] == "") return ""; 
					$sql = "SELECT ";
					$sql .= $obj['cols'];
					$sql .= " FROM ".$obj['tables']." ";
					if ($obj['where'] != "" || isset($obj['where'])) $sql .= " WHERE ".$obj['where']; 
					return ($sql);
				break;
			case "INSERT":
				if ($obj['cols']=="" || $obj['values']=="" || $obj['tables']=="") return "";
				$sql = "INSERT INTO ";
				$sql .= $obj['tables']." ";
				$sql .= "( ".$obj['cols']." )";
				$sql .= " VALUES ";
				$sql .= "( ".$obj['values']." )";
				return ($sql);
				break;
			case "UPDATE":
				if ($obj['where']=="" || $obj['tables']=="" || $obj['colsAndValues']=="" ) return "";
				$sql = "UPDATE ";
				$sql .= $obj['tables']." ";
				$sql .= " SET ";
				$sql .= $obj['colsAndValues']." ";
				$sql .= " WHERE ";
				$sql .= $obj['where'];
				return ($sql);
				break;
			case "DELETE":
					if ($obj['where']=="" || $obj['tables']=="") return "";
					$sql = "DELETE ";
					$sql .= " FROM ";
					$sql .= $obj['tables']." ";
					$sql .= " WHERE ";
					$sql .= $obj['where'];
					return ($sql);
					break;
			}
		}
	}
?>