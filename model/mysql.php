<?php
	class MySQL Extends DB {
		//inserting record
		function INSERT($cols, $tables, $values) {
			global $dbConn;
			$obj = array("op"=> "INSERT", "cols"=> $cols, "tables"=> $tables, "values"=>$values);
			$sql = parent::buildSQL($obj);
			$rs = $dbConn->prepare($sql);
			$rs->execute();
			return $rs;
		}
		//updating record
		function UPDATE($columnsAndValues, $tables, $where) {
			global $dbConn;
			$obj = array("op"=>"UPDATE", "colsAndValues"=>$columnsAndValues, "tables"=>$tables, "where"=>$where);
			$sql = parent::buildSQL($obj);
			$rs = $dbConn->prepare($sql);
			$rs->execute();
			return $rs;
		}
		//selecting record
		function SELECT($cols, $tables, $where) {
			global $dbConn;
			$resultsToArray = array();
			$obj = array('op'=>'SELECT', 'cols'=>$cols, 'tables'=>$tables, 'where'=>$where);
			$sql = parent::buildSQL($obj);
			$rs = $dbConn->prepare($sql);
			$rs->execute();
			while ($row = $rs->fetch())  {
				array_push($resultsToArray, $row);
			}
			return $resultsToArray;
		}
		//deleting record
		function DELETE($tables, $where) {
			global $dbConn;
			$obj = array("op"=>"DELETE", "tables"=>$tables, "where"=>$where);
			$sql = parent::buildSQL($obj);
			$rs = $dbConn->prepare($sql);
			$rs->execute();
			return $rs;
		}
		//check record exist
		function RecordExist($cols, $tables, $where) {
			global $dbConn;
			$obj = array('op'=>'SELECT', 'cols'=>$cols, 'tables'=>$tables, 'where'=>$where);
			$sql = parent::buildSQL($obj);
			$rs = $dbConn->query($sql);
			$num_rows = ($rs->rowCount() > 0) ? true : false;
			echo $num_rows;
			return $num_rows;
		}
		
		//Execute query and return ID
		function InsertReturnKey($cols, $tables, $values) {
			global $dbConn;
			$obj = array("op"=> "INSERT", "cols"=> $cols, "tables"=> $tables, "values"=>$values);
			$sql = parent::buildSQL($obj);
			$rs = $dbConn->prepare($sql);
			$rs->execute();
			$key =  $dbConn->lastInsertId();
			return $key; 
		}
		
		//Execute query and return ID
		function ExecuteSql($sql) {
			global $dbConn;
			$result = mysql_query($sql);
			$key = mysql_insert_id($dbConn);
			return $result; 
		}

	}
?>