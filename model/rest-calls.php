<?php
	function rmplCallPOST($rumpleRoute, $json) {
	
		// JSON and Apikey Headers
		//$rumpleCallHeaders[] = "Apikey: " . $GLOBALS['rumpleApiKey'];
		$rumpleCallHeaders[] = "Content-Type: application/json;charset=utf-8";
	
		// Call the API
		$rumpleCall = curl_init();
		curl_setopt($rumpleCall, CURLOPT_URL, "http://localhost/createcard");
		curl_setopt($rumpleCall, CURLOPT_RETURNTRANSFER, true);
		
		// Get the response
		$response = curl_exec($rumpleCall);
	
		// Close cURL connection
		curl_close($rumpleCall);
	
		// Decode the response (Transform it to an Array)
		$response = json_decode($response, true);
	
		// Return response
		return $response;
	}
	
	// All Admin Leads
	function rmplCreateCard($json) {
		// Make the Call
		$response = rmplCallPOST("createcard", $json);

		// Return Response
		return $response;
	}



?>