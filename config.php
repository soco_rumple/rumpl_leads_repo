<?php
	static $siteState = "Local";

	static $pageIncludeKey = "e36EA&OprrrZ*xFUWFpOHyFE+LZzo^!Ei0jiwkdGk4L*h4S@enxssb!j4yRUglNe";
	switch ($siteState) {
		case "Local":
			$DS = "/";
			$pathSiteURL = "http://localhost";
			$pathSite = $_SERVER['DOCUMENT_ROOT'];
			break;	
		case "Online":
			break;	
		default:
			break;
	}

	//page extentions
	$page_extension = ".page.";
	$page_include = ".inc.";
	$page_ajax = ".ajax.";
	
	//mvc paths
	$controller_path = $pathSite.'controllers'.$DS;
	$model_path = $pathSite.'models'.$DS;
	$view_path = $pathSite.'views'.$DS;

	$public_path = $pathSite.'public'.$DS;
	
	//$rumpleApiUrl = "https://api.rumple.com/v1/";
	$rumpleApiUrl = "";
	$rumpleApiKey = "TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ";
	
	// Rumple API URL
	/*$rumpleApiUrl = "https://api.rumple.com/v1/"; // The Rumple API URL
	
	if($_SERVER["SERVER_ADDR"] != "173.255.204.8") {
		$rumpleApiUrl = "http://dev.rumple.com/api/v1/"; // The Rumple API URL
	}*/
	
	if ($pathSite=="") die("Undefined Root Path");
	
	//routing page requests
	$url = $_SERVER['REQUEST_URI'];
	$url = str_replace('?'.$_SERVER['QUERY_STRING'], '', $url);	
	$routes = array(
					array('url'=>'/^\/?$/', 'controller'=>'pages', 'view'=>'index'),
					array('url'=>'/^\/rumple-call$/', 'controller'=>'pages', 'view'=>'rumple-call'),
					array('url'=>'/^\/tables$/', 'controller'=>'pages', 'view'=>'tables'),
					
					//API requests 
					array('url'=>'/^\/deleteleadnotes$/', 'controller'=>'pages', 'view'=>'deleteleadnotes'),
					array('url'=>'/^\/getleadnotes$/', 'controller'=>'pages', 'view'=>'getleadnotes'),
					array('url'=>'/^\/createleadnotes$/', 'controller'=>'pages', 'view'=>'createleadnotes'),
					array('url'=>'/^\/getlead$/', 'controller'=>'pages', 'view'=>'getlead'),
					array('url'=>'/^\/createlead$/', 'controller'=>'pages', 'view'=>'createlead'),
					array('url'=>'/^\/updateOrdinalPosition$/', 'controller'=>'pages', 'view'=>'updateOrdinalPosition'),
					array('url'=>'/^\/createcard$/', 'controller'=>'pages', 'view'=>'createcard'),
					array('url'=>'/^\/gettables$/', 'controller'=>'pages', 'view'=>'gettables'),
					
					);
	$match = false;
	$params= array();
	foreach($routes as $val => $key) {
		if(preg_match($key['url'], $url, $matches)) {
			$params = array_merge($matches, $params);
			$match = true;
			break;
		}

	}
	if (!$match) { header('location:'.$pathSiteURL);} //if no match found redirect to the index page
?>