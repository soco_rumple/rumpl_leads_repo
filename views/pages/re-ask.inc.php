<div id="re-ask-container" class="row non-action-leads" style=" width:700px; margin:0 auto; display:none">
	<h2 style="text-align:center">Re-Ask</h2>
	
    <div class="col-md-12">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="" data-toggle="collapse" data-target="#this-week-Continuances" style="background-color:#8b8b8b; border-color:#8b8b8b ">
                    <table class="table" style="margin:0; ">
                        <tr>
                            <td class="pull-left" style="font-size:16px; color:#FFF; border:0">Continuences That Need To Be Repitched</td>
                            <td class="pull-right" style="font-size:16px; color:#FFF; border:0">$8,800</td>
                        </tr>
                    </table>
                </div>
                <div id="this-week-Continuances" class="panel-collapse collapse in">
                    <div class="panel-body" style="border:thin #666 solid; border-top:0; background:#FFF; font-family: 'Roboto', sans-serif; padding:0 !important">
                    	<div class="" data-toggle="collapse" data-target="" style="padding:0;  border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                                <tr>
                                    <td class="col-md-6" style="text-align:center; color:#333; font-size:15px; padding:15px">Asks: $8,800</td>
                                    <td class="col-md-6" style="text-align:center; padding:15px; font-size:15px;"><span class="rumple-green">Closing Ratio: 45%</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="" style="padding:0;  border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-md-4" style="text-align:left; color:#333;  padding:5px">Name</td>
                                    <td class="col-md-4" style="text-align:center; padding:5px"><span class="rumple-green">Ask</span></td>
                                    <td class="col-md-4" style="text-align:center;"><span class="rumple-orange">Close</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="" data-toggle="collapse" data-target="#ask-don-Hambly" style="padding:0; cursor:pointer; border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don Hambly</td>
                                    <td class="col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-md-4"></td>
                                </tr>
                            </table>
                        </div>
             			<div class="" data-toggle="collapse" data-target="#ask-evan-almighty" style="padding:0; cursor:pointer;  border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Evan Almighty</td>
                                    <td class="col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-md-4"></td>
                                </tr>
                            </table>
                        </div>
                        <div id="ask-evan-almighty" class="panel-collapse collapse in"  >
                            <div class="col-md-12" style="padding:10px 0; border-bottom:thin #CCC solid">
                       			<div class="col-md-3"><a href="#" type="button" class="btn btn-xs btn-dim-gray" style="width:120px;"><span class="action-icon"> View Action Card</span></a></div>
                                <div class="col-md-3"><a href="#" type="button" class="btn btn-xs btn-dim-gray" style="width:120px;">Archive Lead</a></div>
                                <div class="col-md-3"><a href="#" type="button" class="btn btn-xs btn-rumple" style="width:120px; font-weight:normal;"><span class="ask-this-week-icon">Ask This Week</a></div>
                                <div class="col-md-3"><a href="#" type="button" class="btn btn-xs btn-rumple" style="width:120px; font-weight:normal;"><span class="ask-next-week-icon">Ask Next Week</a></div>
                            </div> 
                        </div>
                        <div class="" data-toggle="collapse" data-target="#ask-don-Hambly" style="padding:0; cursor:pointer; border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don Hambly</td>
                                    <td class="col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-md-4"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

</div>