<div id="weekly-history-container" class="row non-action-leads" style=" max-width:600px; margin:0 auto; display:none">
	<h2 style="text-align:center">Weekly History</h2>
	
    <div class="col-md-12"  style="margin-bottom:50px;">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="#weekly-history-last-week" style="height:55px; background-color:#8b8b8b; border-top:thin #666 solid; border-left:thin #666 solid; border-right:thin #666 solid">
                    <ul class="list-inline" style="margin-bottom:0">
                    	<li class="pull-left"><h1 class="panel-title" style="position: relative; top: 5px; font-size:22px; font-weight:bold; color:#FFF;" >Last Week</h1></li>
                        <li class="pull-right">
                        	<ul class="list-unstyled" style="margin-bottom:0; font-size:12px;">
                            	<li>Weekly Goal: $8,800</li>
                                <li>Closed: <span class="rumple-orange">$2,500</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="weekly-history-last-week" class="panel-collapse collapse in">
                    <div class="panel-body" style="border:thin #666 solid; background:#FFF; font-family: 'Roboto', sans-serif; padding:5px 0 0 0 !important">
                        <div class="col-xs-12 col-sm-12 col-md-12" style="padding:0 0 5px 0;">
                            <div class="col-xs-4 col-md-4" style="font-size:13px; text-align:center;">Rumple Number: <span class="rumple-green">$8,800</span></div>
                            <div class="col-xs-4 col-md-4" style="font-size:13px; text-align:center;">Ask $3,615</div>
                            <div class="col-xs-4 col-md-4" style="font-size:13px; text-align:center;">Closing Ratio: 58%</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="border-top:thin #333 solid;">
                            <div class="col-xs-4 col-md-4" style="font-size:13px; padding:5px 0; text-align:left; font-weight:bold; color:#888;">Name</div>
                            <div class="col-xs-4 col-md-4" style="font-size:13px; padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-green">Ask</span></div>
                            <div class="col-xs-4 col-md-4" style="font-size:13px; padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-orange">Close</span></div>
                    	</div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="border-top:thin #333 solid; font-size:12px;">
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:left; border-right:thin #333 solid; color:#888;"><i class="fa fa-square rumple-green" aria-hidden="true"></i> Don Hambly</div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold; border-right:thin #333 solid;"><span class="rumple-green">$600</span></div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-orange">$550</span></div>
                    	</div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="border-top:thin #333 solid; font-size:12px;">
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:left; border-right:thin #333 solid; color:#888;"><i class="fa fa-square rumple-green" aria-hidden="true"></i> Tyron McKenzie</div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold; border-right:thin #333 solid;"><span class="rumple-green">$600</span></div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-orange">$550</span></div>
                    	</div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="border-top:thin #333 solid; font-size:12px;">
                            <div class="col-xs-4 col-md-4" style=" padding:5px 0; text-align:left; border-right:thin #333 solid; color:#888;"><i class="fa fa-square rumple-green" aria-hidden="true"></i> Evan Almighty</div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold; border-right:thin #333 solid;"><span class="rumple-green">$600</span></div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-orange">$550</span></div>
                    	</div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="border-top:thin #333 solid; font-size:12px;">
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:left; border-right:thin #333 solid; color:#888;"><i class="fa fa-square rumple-green" aria-hidden="true"></i> Holden Thatcher</div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold; border-right:thin #333 solid;"><span class="rumple-green">$600</span></div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-orange">$550</span></div>
                    	</div>
                        <div class="col-xs-12 col-sm-12 col-md-12" style="border-top:thin #333 solid; font-size:14px;">
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:left; font-weight:bold;"></div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold; border-right:thin #333 solid;"><span class="rumple-green">$2,400</span></div>
                            <div class="col-xs-4 col-md-4" style="padding:5px 0; text-align:center; font-weight:bold;"><span class="rumple-orange">$2,400</span></div>
                    	</div>
                    </div>
                </div>
           	</div> 
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="#10Oct16" style="height:55px; background-color:#8b8b8b; border-top:thin #666 solid; border-left:thin #666 solid; border-right:thin #666 solid">
                    <ul class="list-inline" style="margin-bottom:0">
                    	<li class="pull-left"><h1 class="panel-title" style="position: relative; top: 5px; font-size:22px; font-weight:bold; color:#FFF;" >10/Oct/16</h1></li>
                        <li class="pull-right">
                        	<ul class="list-unstyled" style="margin-bottom:0; font-size:12px;">
                            	<li>Weekly Goal: $8,800</li>
                                <li>Closed: <span class="rumple-orange">$2,500</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="10Oct16" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Bootstrap is a powerful front-end framework for faster and easier web development.</p>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="#03Oct16" style="height:55px; background-color:#8b8b8b; border-top:thin #666 solid; border-left:thin #666 solid; border-right:thin #666 solid">
                    <ul class="list-inline" style="margin-bottom:0">
                    	<li class="pull-left"><h1 class="panel-title" style="position: relative; top: 5px; font-size:22px; font-weight:bold; color:#FFF;" >03/Oct/16</h1></li>
                        <li class="pull-right">
                        	<ul class="list-unstyled" style="margin-bottom:0; font-size:12px;">
                            	<li>Weekly Goal: $8,800</li>
                                <li>Closed: <span class="rumple-orange">$2,500</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="03Oct16" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Bootstrap is a powerful front-end framework for faster and easier web development.</p>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="#26Sept16" style="height:55px; background-color:#8b8b8b; border-top:thin #666 solid; border-left:thin #666 solid; border-right:thin #666 solid">
                    <ul class="list-inline" style="margin-bottom:0">
                    	<li class="pull-left"><h1 class="panel-title" style="position: relative; top: 5px; font-size:22px; font-weight:bold; color:#FFF;" >26/Sept/16</h1></li>
                        <li class="pull-right">
                        	<ul class="list-unstyled" style="margin-bottom:0; font-size:12px;">
                            	<li>Weekly Goal: $8,800</li>
                                <li>Closed: <span class="rumple-orange">$2,500</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="26Sept16" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p>Bootstrap is a powerful front-end framework for faster and easier web development.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>