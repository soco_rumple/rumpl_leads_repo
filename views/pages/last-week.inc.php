<div id="last-week-container" class="col-md-12 non-action-leads" style="margin-bottom:220px; display:none">
	<h2 style="text-align:center">Last Week</h2>
	<div style="margin:0 auto; text-align: center; margin-bottom:5px;">
    	<div id="last-week-count"></div>
    	<h4 style="font-family: 'Roboto', sans-serif; color:#666;">THIS WEEKS GOAL: $5,000</h4>
    </div>
    <hr/>
    <div class="row" style="width:600px; max-width:600px; margin:0 auto;">
        <div class="panel-group" id=""> 
            <!-- ASKS PANEL STARTS HERE -->
            <div class="panel panel-default" style="border-color:#259866">
                <div class="" data-toggle="collapse" data-target="#asks" style=" padding:5px; height:35px; background:#259866; border-color:#259866 ">
                    <ul class="list-inline" style="margin-bottom:0">
                    	<li class="pull-left" style="font-size:16px; color:#FFF;">Asks</li>
                        <li class="pull-right" style="font-size:16px; color:#FFF;">$8,800</li>
                    </ul>
                </div>
                <div id="asks" class="panel-collapse collapse in">
                    <div class="panel-body" style="background:#FFF; padding-bottom:0 !important">
						<div class="row">
                        	<ul class="list-inline">
                                <li class="col-xs-6 col-md-6">	
                                    <table class="table" style="border:0">
                                        <tr><td style="text-align:right">Asks:</td><td><span class="rumple-green">$8,800</span></td></tr>
                                        <tr><td style="text-align:right">Need To Ask:</td><td>$9,615</td></tr>
                                        <tr><td style="text-align:right">Still Need:</td><td><span class="rumple-red">$815</span></td></tr>
                                    </table>
                                </li>
                                <li class="col-xs-5 col-md-5">
                                    <table class="table" style="border:0">
                                        <tr><td style="text-align:right">Closed:</td><td><span class="rumple-orange">$2,500</span></td></tr>
                                        <tr><td style="text-align:right">Goal:</td><td>$9,615</td></tr>
                                        <tr><td style="text-align:right">Closing Ratio:</td><td><span class="rumple-green">$815</span></td></tr>
                                    </table>
                                </li>
                           </ul>
                        </div>
      					
                        <!-- SELECT LEAD -->
                        <div class="row" style="margin-bottom:20px;">
                          <div class="col-sm-12">
                            <div class="input-group dropdown">
                              <input type="text" class="form-control input-select-lead dropdown-toggle" style="background:#FFF; border-right:0; box-shadow:none; " value="Select Lead">
                              <ul id="select-lead" class="dropdown-menu col-sm-12" style="border-radius: 0; border-top: 0 none; box-shadow: none !important; top: 30px; width: 99.6%;">
                       			<li><a href="#"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don harper</a></li>
                                <li><a href="#"><i class="fa fa-square" aria-hidden="true" style="color:#445f53" ></i> Everatte Gascia</a></li>
                                <li><a href="#"><i class="fa fa-square" aria-hidden="true" style="color:#e8dd53" ></i> Hanson Hannable</a></li>
                                <li><a href="#"><i class="fa fa-square" aria-hidden="true" style="color:#ff9d00" ></i> Tommy Thompson</a></li>
                                <li><a href="#"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Zig Ziglar</a></li>
                              </ul>
                              <span role="button" class="input-group-addon dropdown-toggle" style="background:#FFF; border-left:0; position:relative; left:-2px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></span>
                            </div>
                          </div>
                        </div>
                        
                        
                        <!-- THEADING -->
                       	<div class="" style="padding:0; margin-left:-10px; margin-right:-10px; font-size:14px; text-align:center; padding-bottom:5px;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-xs-4 col-md-4" style="text-align:left; padding-left:10px;">Name</td>
                                    <td class="col-xs-4 col-md-4"><span class="rumple-green">Ask</span></td>
                                    <td class="col-xs-4 col-md-4"><span class="rumple-orange">Close</span></td>
                                </tr>
                            </table>
                    	</div>
                        
                        <div style="clear:both"></div>   
                        
                        <!-- ACCORDION 1 -->
                        <div class="" data-toggle="collapse" data-target="#last-week-ask-don-Hambly" style="cursor:pointer; padding:0; background:#dbefe6; margin-left:-10px; margin-right:-10px; border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-xs-4 col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don Hambly</td>
                                    <td class="col-xs-4 col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-xs-4 col-md-4"></td>
                                </tr>
                            </table>
                        </div>
                        <!-- COLLAPSIBLE 1-->
                        <div id="last-week-ask-don-Hambly" class="panel-collapse collapse "  >
                            <div class="col-xs-12 col-sm-12 col-md-12" style="padding-bottom:10px; padding-top:10px;">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group" style="text-align:center">
                                        <label style="font-weight:normal; font-size:13px; color:#666;">Ask Type</label>
                                        <div class="input-group spinner" style="text-align:center; margin:0 auto">
                                            <input type="text" class="input-sm form-control" placeholder="Monthly" value="" style="color:#333; width:150px; text-align:center" >
                                            <div class="input-group-btn-vertical">
                                                <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                                                <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                                            </div>
                                        </div>
                                   </div>
                                   <div class="col-md-12"><a href="#" class="btn btn-sm btn-default btn-dim-gray" style="color:#FFF; font-weight:bold; width:120px; margin:0 auto" >View Action Card</a></div>
                                </div>
                                
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group" style="text-align:center">
                                        <label style="font-weight:normal; font-size:13px; color:#666;">Ask</label>
                                        <div class="input-group spinner" style="text-align:center; margin:0 auto">
                                            <input type="text" class="input-sm form-control" placeholder="600" value="" style="color:#333; width:150px; text-align:center; margin:0 auto">
                                        </div>
                                   </div>
                                   <div class="col-md-12"><a href="#" class="btn btn-sm btn-default btn-dim-gray" style="color:#FFF;  font-weight:bold; width:120px; margin:0 auto" >View Action Card</a></div>
                                </div>
                                
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group" style="text-align:center">
                                        <label style="font-weight:normal; font-size:13px; color:#666;">Close</label>
                                        <div class="input-group spinner" style="text-align:center; margin:0 auto">
                                            <input type="text" class="input-sm form-control" placeholder="-" value="" style="color:#333; width:150px; text-align:center; margin:0 auto">
                                        </div>
                                   </div>
                                   <div class="col-md-12"><a href="#" class="btn btn-sm btn-default btn-rumple" style="color:#FFF; font-weight:bold;  width:120px; margin:0 auto" >View Action Card</a></div>
                                </div>
                            </div> 
                        </div>
                        
   						<div style="clear:both"></div>     
                          
                  		<!-- ACCORDION 2 -->
                        <div class="panel-heading" data-toggle="collapse" data-target="#" style="padding:0; background:#dbefe6; margin-left:-10px; margin-right:-10px; border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-xs-4 col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don Hambly</td>
                                    <td class="col-xs-4 col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-xs-4 col-md-4"></td>
                                </tr>
                            </table>
                        </div>
                        
                        <!-- ACCORDION 3 -->
                        <div class="panel-heading" data-toggle="collapse" data-target="#" style="padding:0; background:#dbefe6; margin-left:-10px; margin-right:-10px; border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-xs-4 col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don Hambly</td>
                                    <td class="col-xs-4 col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-xs-4 col-md-4"></td>
                                </tr>
                            </table>
                        </div>
                        <!-- ACCORDION 4 -->
                        <div class="panel-heading" data-toggle="collapse" data-target="#" style="padding:0; background:#dbefe6; margin-left:-10px; margin-right:-10px; border-left:0; border-right:0; border-radius:0; border-top:thin #b8c3bf solid; border-bottom:thin #b8c3bf solid;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-xs-4 col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"><i class="fa fa-square" aria-hidden="true" style="color:#259866" ></i> Don Hambly</td>
                                    <td class="col-xs-4 col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px"><span class="rumple-green">$600</span></td>
                                    <td class="col-xs-4 col-md-4"></td>
                                </tr>
                            </table>
                        </div>
                        <!-- ACCORDION 5 -->
                        <div class="panel-heading" data-toggle="collapse" data-target="#" style="padding:0; background:#FFF; margin-left:-10px; margin-right:-10px; border-left:0; border-right:0; border-radius:0;">
                            <table class="table" style="margin:0; ">
                            	<tr>
                                	<td class="col-xs-4 col-md-4" style="text-align:left; color:#333; border-right:thin #CCC solid; padding:5px"></td>
                                    <td class="col-xs-4 col-md-4" style="text-align:center; border-right:thin #CCC solid; padding:5px; font-weight:bold;"><span class="rumple-green">$2,400</span></td>
                                    <td class="col-xs-4 col-md-4" style="text-align:center; padding:5px; font-weight:bold;"><span class="rumple-orange">$2,400</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







