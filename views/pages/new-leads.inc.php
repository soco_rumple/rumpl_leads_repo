<div class="panel panel-default card-panel">
    <div class="panel-heading stats-panel-heading" style="height:40px;">
    	<span class="pull-left">New Leads(10)</span> 
    </div>
    <div id="drp-tbl-opt" class="col-md-12 dropdown" style="top:-30px;height:2px; float:right">
        <i class="icon-ellipsis-horizontal icon-large table-options pull-right" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
        <ul class="dropdown-menu card-dropdown-menu" aria-labelledby="">
            <div class="arrow-up"></div>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position:relative; top:-5px; left:-10px;" ><span aria-hidden="true">&times;</span></button>
            <li><h4 style="color:#333; text-align:center; font-size:14px;">Table Options</h4></li>
            <li role="separator" class="divider" style="position:relative; left:5px"></li>
            <li><a href="#">Send Email To Table</a></li>
            <li><a href="#">Send Text To Table</a></li>
            <li><a href="#">Send Drop To Table</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Move Leads To Another Table</a></li>
            <li><a href="#">Move Leads To Another User</a></li>
            <li role="separator" class="divider"></li>
            <li>
                <ul class="list-inline" style="padding:0 20px;">
                    <li class="pull-left"><span class="gray-text">Recolor Table</a></li>
                    <li class="pull-right">
                        <div id="drp-clr-opt" class="dropdown">
                            <button id="" type="button" class="btn btn-default btn-table-options-dropdown dropdown-toggle" data-toggle="dropdown" ><span class="caret"></span></button>
                            <ul id="option-colors" class="dropdown-menu scrollable-menu " role="menu" style="">
                                <li><div id="17975f"></div></li>
                                <li><div id="fd5151"></div></li>
                                <li><div id="5c74f8"></div></li>
                                <li><div id="51dcfd"></div></li>
                                <li><div id="fdb451"></div></li>
                                <li><div id="fd7f51"></div></li>
                            </ul>
                        </div>
                    </li>
                </ul>
                
            </li>
            <li><a href="#">Archieve Table</a></li>
        </ul>
    </div>
    <div style="clear:both"></div>
    <div class="panel-body card-panel-body">
         <ul class="list-group droppable">
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 5</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="month-day" >Jun 26</p>
                    <p class="year">2016</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px;">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </li>
        </ul>
    </div>
</div>