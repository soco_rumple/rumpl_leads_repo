<!DOCTYPE html>
<html>
 <head>
   <title>Rumple Twilio Client</title>
   <script type="text/javascript" src="//media.twiliocdn.com/sdk/js/client/v1.3/twilio.min.js"></script>
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
   <link href="//static0.twilio.com/resources/quickstart/client.css" type="text/css" rel="stylesheet" />
   <style>
     body{
       background: #fff;
       width: 600px;
       height: 200px;
     }
   </style>
   <script type="text/javascript">
     Twilio.Device.setup("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZSI6InNjb3BlOmNsaWVudDpvdXRnb2luZz9hcHBTaWQ9QVA1M2Y5N2QyYTRjOGY0ZDk4ZWM4ZmVmNzQyMjg4MzJlMCZhcHBQYXJhbXM9IiwiaXNzIjoiQUNlZTY4N2Q3ZjVlYjY3YmM4OWI3ZTU0MjE2YzNlOTg5MCIsImV4cCI6MTQ3NzQ0ODM1NX0.g8ll9hOpVa3c2Z0dHjwLjtechxHxMn5HWwjgx04dKTE");

     Twilio.Device.ready(function (device) {
       $("#log").text("Ready");
     });

     Twilio.Device.error(function (error) {
       $("#log").text("Error: " + error.message);
     });

     Twilio.Device.connect(function (conn) {
       $("#log").text("Successfully established call");
     });

     Twilio.Device.disconnect(function (conn) {
       $("#log").text("Call ended");
     });

     Twilio.Device.incoming(function (conn) {
       $("#log").text("Incoming connection from " + conn.parameters.From);
       // accept the incoming connection and start two-way audio
       conn.accept();
     });

     function call() {
       // get the phone number to connect the call to
       params = {"PhoneNumber": $("#number").val()};
       Twilio.Device.connect(params);
     }

     function hangup() {
       Twilio.Device.disconnectAll();
     }
   </script>
 </head>
 <body>
   <button class="call" onclick="call();">
     Call
   </button>

   <button class="hangup" onclick="hangup();">
     Hangup
   </button>

   <input type="text" id="number" name="number" value="+18433639956" placeholder="+8433639956" />

   <div id="log">Loading pigeons...</div>
 </body>
</html>