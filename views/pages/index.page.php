<?php error_reporting(0); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Rumple Leads</title>

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet"> 
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo $pathSiteURL ?>/public/css/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $pathSiteURL ?>/public/summernote/summernote.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $pathSiteURL ?>/public/css/bootstrap-datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo $pathSiteURL ?>/public/css/jquery.flipcountdown.css"/>

<link rel="stylesheet" type="text/css" href="../../public/css/index.css"/>
</head>

<body>
    <div id="header" class="panel panel-default">
        <div class="panel-body" style="padding:0 0 0 5px;">
            <h3><span class="rumple-text-bg">rumple</span> <span class="salesforce-text-bg">sales force</span></h3>
        	<div class="col-md-6 pull-right" style="padding-right:0" >
            	<div class="col-md-2 pull-right" style="padding-bottom: 10px; padding-top: 8px; width: 98px;">
                    <a id="btn-profile-modal" href="#profileModal"><img id="profile-img" src="../../public/images/1x/group-321.png"  style="" /></a>
                    <div class="btn-group">
                        <a id="profile-text" href="#" class=" dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        	Josh Krebs<span class="caret caret-black"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#">Account Settings</a></li>
                            <li><a href="#">My Demo Schedule</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Help</a></li>
                            <li><a href="#">Log Out</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 pull-right" style="width:97px; padding-right:0">
                    <ul class="list-inline" style="margin-bottom:0;">
                        <li>
                            <a id="btn-schedule-modal" href="#schedule-modal"><img id="calendar-img" src="../../public/images/1x/group-162.png" /></a>
                            <span class="badge badge-notify-calendar">1</span>
                        </li>
                        <li>
                            <a id="reminders" href="#reminders-modal"><img id="alarm-img" src="../../public/images/1x/path-1.png"  style=" " /></a>
                            <span class="badge badge-notify-alarm">1</span>
                        </li>
                       <!-- <li>
                            <a id="profile-modal" href="#profileModal"><img id="profile-img" src="../../public/images/1x/group-321.png"  style="" /></a>
                            <p id="profile-text" >Josh Krebs<span class="caret caret-black"></span></p>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <!-- SCHEDULE MODAL -->
    <div id="schedule-modal" class="modal fade">
        <div class="container-fluid modal-dialog" style="max-width:700px; color:#666 !important;">
            <div class="row modal-content">
                <div class="modal-header" style="padding:15px 15px 0 15px !important">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <ul id="schedule-modal-head" class="list-inline">
                    	<li><img id="profile-img" src="../../public/images/1x/group-321.png"  style="" /></li>
                        <li>
                        	<h5 class="modal-title">Josh Krebs</h5>
                    		<p style="font-size:10px;">Rumple Salesteam</p>
                  		</li>
                    </ul>
                    <div class="col-md-12" style="border-bottom:2px #CCC solid; padding-left:0"><h4>My Demo Schedule</h4></div>
                </div>
                <div class="modal-body" style="padding-bottom:30px; padding-top:0">
                    <ul class="list-inline">
                        <li><h4 style="position:relative;top: 5px;">Demo Reminders</h4></li>
                        <li><div class="checkbox"><label><input type="checkbox" value=""><span class="small-font-11 checkbox-padding-11">Emails</span></label></div></li>
                        <li><div class="checkbox"><label><input type="checkbox" value=""><span class="small-font-11 checkbox-padding-11">Texts</span></label></div></li>
                    </ul>
                    <hr/>
                    <p>Click Meeting To Go To Card</p>
                    <div class="col-md-12" style="padding-left:0; padding-right:0">
                        <table id="profile-schedule-time" class="table table-bordered" style="font-size:13px">
                            <thead>
                                <tr>
                                    <th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Harper</th><th>Friday</th><th>Saturday</th><th>Sunday</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                                </tr>
                                <tr>
                                    <td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td>
                                </tr>
                                <tr>
                                    <td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td>
                                </tr>
                                <tr>
                                    <td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td>
                                </tr>
                                <tr>
                                    <td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td>
                                </tr>
                                <tr>
                                    <td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td>
                                </tr>
                                <tr>
                                    <td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td>
                                </tr>
                                <tr>
                                    <td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td>
                                </tr>
                                <tr>
                                    <td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- PROFILE MODAL -->
    <div id="profileModal" class="modal fade">
        <div class="container-fluid modal-dialog" style="max-width:700px">
            <div class="modal-content">
                <div class="modal-header" style="padding:15px 15px 0 15px !important">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <ul id="profile-head" class="list-inline">
                    	<li><img id="profile-img" src="../../public/images/1x/group-321.png"  style="" /></li>
                        <li>
                        	<h5 class="modal-title">Josh Krebs</h5>
                    		<p style="font-size:10px;">Rumple Salesteam</p>
                  		</li>
                    </ul>
                    <div class="col-md-12" style="border-bottom:2px #CCC solid; padding-left:0"><h4>Account Settings</h4></div>
                </div>
                <div class="modal-body" style="padding-bottom:30px; padding-top:0">
                	<div id="profile-outer-container">
                        <div id="profile-inner-container" class="row">
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding: 0;">
                                <ul id="profile-name-contact" class="list-unstyled">
                                    <li><h4>Josh Krebs</h4></li>
                                    <li><h4>435-817-8201</h4></li>
                                    <li><h4>josh@rumple.com</h4></li>
                                </ul>
                            </div>
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding:0;">
                                <ul id="profile-upload" class="list-inline">
                                    <li><h4>Upload A Picture(2MB)</h4></li>
                                    <li><span class="btn btn-default btn-file" style="padding:2px"><input type="file" style="border-radius: 2px; color: #666; font-size: 11px; padding: 0;"></span></li>
                                </ul>
                            </div>
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding: 0;">
                                <ul id="profile-timezone" class="list-inline">
                                    <li><h4>Timezone</h4></li>
                                    <li>
                                        <select class="form-control" id="opt-timezone">
                                            <option selected="selected">(-07:00)America/Denver</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding: 0;">
                                <ul id="profile-google-calendar" class="list-inline">
                                    <li><h4>Google Calendar</h4></li>
                                    <li><input name="" class="btn btn-xs btn-default btn-gray" type="button" value="Connect Now" /></li>
                                </ul>
                            </div>
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding: 0;">
                                <ul id="profile-change-password" class="list-inline">
                                    <li><h4>Change Password</h4></li>
                                    <li><input name="" class="btn btn-xs btn-default btn-gray" type="button" value="Change Now" /></li>
                                </ul>
                            </div>
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding: 0;">
                                <ul id="profile-notification-settings" class="list-inline">
                                    <li><h4>Notification Settings</h4></li>
                                    <li>
                                        <ul class="list-inline">
                                            <li><div class="checkbox"><label><input type="checkbox" value=""><span class="small-font-11 checkbox-padding-11">Emails</span></label></div></li>
                                            <li><div class="checkbox"><label><input type="checkbox" value=""><span class="small-font-11 checkbox-padding-11">Texts</span></label></div></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9" style="border-bottom:1px #CCC solid; padding:0 45px; margin: 0 15px; padding: 0;">
                                <ul id="profile-my-demo-schedule" class="list-inline">
                                    <li><h4>My Demo Schedule</h4></li>
                                    <li><div class="checkbox"><label><input type="checkbox" value=""><span class="small-font-11 checkbox-padding-11">I Accept Demos With Clients</span></label></div></li>
                                </ul>
                            </div>
                            <div class="col-md-12" style="margin-top:20px">
                            	<p>Schedule the time you are normally available during the week.</p>
                            	<table id="profile-schedule-time" class="table table-bordered" style="font-size:13px">
                             		<thead>
                                    	<tr>
                                            <th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Harper</th><th>Friday</th><th>Saturday</th><th>Sunday</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<tr>
                                        	<td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td><td>7:30 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td><td>8:00 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td><td>9:00 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td><td>9:30 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td><td>10:00 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td><td>10:30 AM</td>
                                        </tr>
                                        <tr>
                                        	<td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td><td>11:00 AM</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- REMINDERS MODAL -->
    <div id="reminders-modal" class="modal fade">
        <div class="modal-dialog" style="max-width:700px">
            <div class="modal-content">
                <div class="modal-header" style="padding:15px 15px 0 15px !important">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <ul id="reminders-head" class="list-inline">
                    	<li><img id="reminders-img" src="../../public/images/1x/path-1.png" style="display:block" /></li>
                        <li>
                        	<h4 class="modal-title" style="position:relative; top:-4px;">Reminders</h4>
                  		</li>
                    </ul>
                    <div class="col-md-12" style="border-bottom:2px #CCC solid; padding-left:0"></div>
                </div>
                <div class="modal-body" style="padding-top:0; padding-bottom:30px; padding-top:0">
                	<div class="row" id="reminders-outer-container">
                        <div id="reminders-inner-container" class="col-md-12">
                    		<div class="col-md-12">
                            	<h4 style="color:#F00; padding-top:0">Today (2)</h4>
                            	<div class="row">
                                	<p>"Call Don, he said he was busy till this day"</p>
                                    <p style="font-size:12px">Card For Tom Hardy - Set For October 24, 2016</p>
                                    <ul class="list-inline">
                                    	<li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />Mark Complete</a></li>
                                        <li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />View Card</a></li>
                                        <li class="pull-right"><p style="font-size:11px;">Set By: Josh October 20, 2016</p></li>
                                    </ul>
                            	</div>
                                <hr/>
                                <div class="row">
                                	<p>"This is a sample of more than one reminder"</p>
                                    <p style="font-size:12px">Card For Tom Hardy - Set For October 24, 2016</p>
                                    <ul class="list-inline">
                                    	<li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />Mark Complete</a></li>
                                        <li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />View Card</a></li>
                                        <li class="pull-right"><p style="font-size:11px;">Set By: Josh October 20, 2016</p></li>
                                    </ul>
                            	</div>
                                <hr/>
                                <h4 style=" padding-top:0;">Overdue (0)</h4>
                                <hr/>
                                <h4 style=" padding-top:0;">Up-Coming (5)</h4>
                                <div class="row">
                                	<p>"This is a sample of more than one reminder"</p>
                                    <p style="font-size:12px">Card For Tom Hardy - Set For October 24, 2016</p>
                                    <ul class="list-inline">
                                    	<li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />Mark Complete</a></li>
                                        <li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />View Card</a></li>
                                        <li class="pull-right"><p style="font-size:11px;">Set By: Josh October 20, 2016</p></li>
                                    </ul>
                            	</div>
                                <hr/>
                                <div class="row">
                                	<p>"This is a sample of more than one reminder"</p>
                                    <p style="font-size:12px">Card For Tom Hardy - Set For October 24, 2016</p>
                                    <ul class="list-inline">
                                    	<li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />Mark Complete</a></li>
                                        <li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />View Card</a></li>
                                        <li class="pull-right"><p style="font-size:11px;">Set By: Josh October 20, 2016</p></li>
                                    </ul>
                            	</div>
                                <hr/>
                                <div class="row">
                                	<p>"This is a sample of more than one reminder"</p>
                                    <p style="font-size:12px">Card For Tom Hardy - Set For October 24, 2016</p>
                                    <ul class="list-inline">
                                    	<li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />Mark Complete</a></li>
                                        <li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />View Card</a></li>
                                        <li class="colpull-right"><p style="font-size:11px;">Set By: Josh October 20, 2016</p></li>
                                    </ul>
                            	</div>
                                <hr/>
                                <div class="row">
                                	<p>"This is a sample of more than one reminder"</p>
                                    <p style="font-size:12px">Card For Tom Hardy - Set For October 24, 2016</p>
                                    <ul class="list-inline">
                                    	<li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />Mark Complete</a></li>
                                        <li><a class="btn btn-xs btn-default " style="width:120px; font-size:11px;" type="button" />View Card</a></li>
                                        <li class="pull-right"><p style="font-size:11px;">Set By: Josh October 20, 2016</p></li>
                                    </ul>
                            	</div>
                                <hr/>
                            </div>
                        </div>
                	</div>
                </div>
            </div>
        </div>
    </div>

	<div id="wrapper" class="col-md-12" >
		<!-- Static navbar -->
        <nav id="top-nav" class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="btn-xs navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <h3 style="position:relative; top:-5px; padding-left:20px; width:300px" ><span class="rumple-text-bg">rumple</span> <span class="salesforce-text-bg">sales force</span></h3>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav" style="background:#45ac7f; border-color:#45ac7f ">
                    <li><a href="#"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
                    <li id="leads" data-toggle="collapse" data-target="#lead-subitems" class=" active"><a href="#">Leads <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a> </li>
                    <ul class="list-unstyled sub-menu collapse in" id="lead-subitems">
                        <li id="actions"><a href="#">Actions</a></li>
                        <li id="this-week"><a href="#">This Week</a></li>
                        <li id="next-week"><a href="#">Next Week</a></li>
                        <li id="last-week"><a href="#">Last Week</a></li>
                        <li id="re-ask"><a href="#">Re-Ask</a></li>
                        <li id="weekly-history"><a href="#">Weekly History</a></li>
                        <li id=""><a href="#"></a></li>
                    </ul>
                    <li id="new-customer" data-toggle="collapse" data-target="#new-customer-subitems" class="collapsed"><a href="#">New Customers <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a></li>
                    <ul class="list-unstyled sub-menu collapse" id="new-customer-subitems">
                        <li id="actions"><a href="#">Actions</a></li>
                        <li id="this-week"><a href="#">This Week</a></li>
                        <li id="next-week"><a href="#">Next Week</a></li>
                        <li id="last-week"><a href="#">Last Week</a></li>
                        <li id="re-ask"><a href="#">Re-Ask</a></li>
                        <li id="weekly-history"><a href="#">Weekly History</a></li>
                        <li id=""><a href="#"></a></li>
                    </ul>
                    <li id="support"><a href="#">Support</a></li>
                    <li id="email-template"><a href="#">Email Templates</a></li>
                    <li id="archive-labels"><a href="#">Archieve Labels</a></li>
                </ul>
              </div>
            </div>
        </nav>
      
    	<!-- LEFT NAVIGATIONS STARTS HERE -->
        <div id="left-nav" class="col-md-2">
            <div class="sidebar">
              <ul class="nav nav-sidebar">
                <li><a href="#"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
            	<li id="leads" data-toggle="collapse" data-target="#lead-subitems" class=" active"><a href="#">Leads <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a> </li>
                <ul class="list-unstyled sub-menu collapse in" id="lead-subitems">
                    <li id="actions"><a  id="lead-action" href="#">Actions</a></li>
                    <li id="this-week"><a href="#">This Week</a></li>
                    <li id="next-week"><a href="#">Next Week</a></li>
                    <li id="last-week"><a href="#">Last Week</a></li>
                    <li id="re-ask"><a href="#">Re-Ask</a></li>
                    <li id="weekly-history"><a href="#">Weekly History</a></li>
                    <li id=""><a href="#"></a></li>
                </ul>
                <li id="new-customer" data-toggle="collapse" data-target="#new-customer-subitems" class="collapsed"><a href="#">New Customers <i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a></li>
                <ul class="list-unstyled sub-menu collapse" id="new-customer-subitems">
                    <li id="actions"><a href="#">Actions</a></li>
                    <li id="this-week"><a href="#">This Week</a></li>
                    <li id="next-week"><a href="#">Next Week</a></li>
                    <li id="last-week"><a href="#">Last Week</a></li>
                    <li id="re-ask"><a href="#">Re-Ask</a></li>
                    <li id="weekly-history"><a href="#">Weekly History</a></li>
                    <li id=""><a href="#"></a></li>
                </ul>
                <li id="support"><a href="#">Support</a></li>
                <li id="email-template"><a href="#">Email Templates</a></li>
                <li id="archive-labels"><a href="#">Archieve Labels</a></li>
              </ul>
            </div>
        </div>
        
        <!-- CARDS CONTENT STARTS HERE -->
        <div id="cards" class="col-md-10 cards-pane" >
       		<div class="col-md-12 well container-head" style="padding:5px; border-radius:0;">
                <div class="col-xs-12 col-sm-5 col-md-4" style="padding-left:0 !important;">
                	<button id="btn-new-lead" type="button" class="btn btn-sm btn-default pull-left" disabled="disabled" >Add New Lead</button>
                	<button id="btn-upload-lead-sheet" type="button" class="btn btn-sm btn-default">Upload Lead Sheet</button>
                    <!--<button id="btn-test-ajax-call" type="button" class="btn btn-sm btn-default">Test Ajax Call</button>-->
                </div>
                <div id="search-container" class="col-xs-6 col-sm-5 col-md-6">
                    <div class="input-group" style="max-width:500px;">
                      <input type="text" class="form-control" placeholder="Search Lead" style="min-width:160px">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button" style="background:#45ac7f; color:#FFF; border-color:#45ac7f" >Search</button>
                      </span>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-2 col-md-2">
                	<div id="btn-card-view" class="btn-group pull-right " role="group" aria-label="">
                      <button id="btn-stats" type="button" class="btn btn-default active "><span class="glyphicon glyphicon-stats" aria-hidden="true"></span></button>
                      <button id="btn-dashboard"  type="button" class="btn btn-default "><span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>
            
			<div class="col-md-12 container-outer">
            	<div class="row container-inner">
                    <!-- Stats tables -->
                    <?php /*?><div class="table-container">
                        <?php include_once($view_path."pages/new-leads.inc.php"); ?>
                    </div>
                    <div class="table-container">
                        <?php include_once($view_path."pages/left-message.inc.php"); ?>
                    </div>
                    <div class=" table-container">
                        <?php include_once($view_path."pages/send-email.inc.php"); ?>
                    </div>
                    <div id="add-table" class="add-table-container">
                        <a id="add-link" href="#">Add a Table</a>
                        <div id="select-container" class="form-group" style="">
                            <div class="btn-group">
                                <button id="option-btn" type="button" class="btn btn-default btn-tbl-colors dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;" ><span class="caret"></span></button>
                                <ul id="option-colors" class="dropdown-menu scrollable-menu" role="menu" style="">
                                    <li><div id="17975f"></div></li>
                                    <li><div id="fd5151"></div></li>
                                    <li><div id="5c74f8"></div></li>
                                    <li><div id="51dcfd"></div></li>
                                    <li><div id="fdb451"></div></li>
                                    <li><div id="fd7f51"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Dashboard tables -->
                    <div class="table-container dashboard-table-container" style="display:none">
                        <?php include_once($view_path."pages/dashboard-new-leads.inc.php"); ?>
                    </div>
                    <div class="table-container dashboard-table-container" style="display:none">
                        <?php include_once($view_path."pages/dashboard-new-leads2.inc.php"); ?>
                    </div>
                    <div class="table-container dashboard-table-container" style="display:none">
                        <?php include_once($view_path."pages/dashboard-new-leads3.inc.php"); ?>
                    </div><?php */?>
                    
                    <!-- This week leads -->
                    <?php include_once($view_path."pages/this-week.inc.php"); ?>
                    
                    <!-- Last week leads -->
          			<?php include_once($view_path."pages/next-week.inc.php"); ?>
                    
                    <!-- Last week leads -->
                    <?php include_once($view_path."pages/last-week.inc.php"); ?>
                    
                    <!-- Re ask leads -->
          			<?php include_once($view_path."pages/re-ask.inc.php"); ?>
                    
                    <!-- Weekly history leads -->
                    <?php include_once($view_path."pages/weekly-history.inc.php"); ?>
                    
                </div>                    
			</div>
           
           <!-- ADD NEW LEAD MODAL -->
                <div id="lead-modal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal">×</button>
                            	<h3 class="modal-title">Add To "New Leads"</h3>
                            </div>
                            <div class="modal-body">
                            	<div class="form-group">
                            		<input type="text" class="form-control" id="lead-name" placeholder = "Lead Name">
                                </div>
                                <div class="form-group">
                                	<input type="text" class="form-control" id="lead-company" placeholder = "Company Name">
                                </div>
                                <div class="form-group">
                                	<input type="text" class="form-control" id="lead-title" placeholder = "Lead Title">
                                </div>
                                <div class="form-group">
                                	<input type="text" class="form-control" id="lead-email" placeholder = "Email">
                                </div>
                                <div class="form-group">
                                	<input type="text" class="form-control" id="lead-phone" placeholder = "Phone">
                                </div>
                                <div class="form-group">
                                	<input type="text" class="form-control" id="lead-mobile" placeholder = "Mobile">
                                </div>
                                <div class="form-group">
                            		<input type="text" class="form-control" id="lead-website" placeholder = "Website">
                                </div>
                                <div class="form-group">
                            		<input type="text" class="form-control" id="lead-address" placeholder = "Address">
                                </div>
                            </div>
                            <div class="modal-footer">
                            	<a href="#" id="btn-save-lead" tableid="" class="btn btn-sm btn-default" style="margin:0 0.1%; width:80px;">Save</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- UPLOAD SHEET MODAL -->
                <div id="upload-sheet-modal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            	<button type="button" class="close" data-dismiss="modal">×</button>
                            	<h3 class="modal-title">Upload Lead Sheet</h3>
                            </div>
                            <div class="modal-body">
                            	<div class="col-md-12" style="background:#eee; border:2px #CCC dotted; border; text-align:center">
                                    <label class="btn btn-sm btn-default btn-file" style=" margin:25%; auto;">
                                        Upload a file <input type="file" style="display: none;"> 
                                    </label>
                                    <p>or drag a file here.</p >
                                	<p id="supported-text" style="font-size:12px; font-weight:normal;">Only Excel(.xls and .xlsx) and .csv file types are supported</p>
                                </div>
                                
                                <button class="btn btn-sm btn-default pull-right" style="position:relative; top:0; margin:1%; width:80px; background:#eee !important;">Next</button>
                            </div>
                            <div style="clear:both"></div>
                            <div class="modal-footer">
                            	<button type="button" class="btn btn-sm btn-default btn-arrow-right" style="min-width:32%; position:relative; left:-15px; background:#eee !important;" >Upload File</button>
                                <button type="button" class="btn btn-sm btn-default btn-arrow-right" style="min-width:32%; position:relative; left:-15px;">Mapping</button>
                                <button type="button" class="btn btn-sm btn-default btn-arrow-right" style="min-width:32%; position:relative; left:-15px;">Preview and Finish</button>
                            </div>
                        </div>
                    </div>
                </div>
                
           		<div id="draggable-card-item-modal" class="modal fade" tabindex="-1" role="dialog"></div>
                
                <!-- Modal Start here-->
                <div class="modal fade bs-example-modal-sm" id="myPleaseWait" tabindex="-1"
                    role="dialog" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">
                                    <span class="glyphicon glyphicon-time">
                                    </span>Please Wait
                                 </h4>
                            </div>
                            <div class="modal-body">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-info
                                    progress-bar-striped active"
                                    style="width: 100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal ends Here -->
                
        </div>
	</div>
 
    
	<script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/jquery.slimscroll.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/summernote/summernote.min.js"></script>
    <script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/jquery.ui.touch-punch.js"></script>
	<script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="<?php echo $pathSiteURL ?>/public/js/jquery.flipcountdown.js"></script>

    <script type="text/javascript" src="../../public/js/index.js"></script>
    
    <script type="text/javascript" src="../../public/js/rmpl-popovers.js"></script>
    <script type="text/javascript" src="../../public/js/rmpl-actions.js"></script>

</body>
</html>
