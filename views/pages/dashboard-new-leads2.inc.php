<div id="" class="panel panel-default card-panel dashboard-card-panel">
    <div class="panel-heading dashboard-panel-heading text-center">
    	<div class="col-md-12 head-title" ><span class="card-title" >October 2016</span> </div>
        <div class="row children-div">
        	<div class="col-xs-4 col-md-4 left-div">
            	<p class="month-to-month" style="">Month To Month:8</p>
                <p class="yearly-contract" style="">Yearly Contracts:9</p>
                <p class="total" style=""><strong>Total: 17</strong></p>
            </div>
            <div class="col-xs-4 col-md-4 center-div">
            	<p class="goal-for-month">Goal For This Month</p>
                <p class="amount">$15,000</p>
            </div>
            <div class="col-xs-4 col-md-4 right-div" style="">
            	<p class="projected" style="">Projected To Close</p>
                <p class="amount" style=" ">$15,000</p>
            </div>
        </div>
	</div>
    <div id="dashboard1" class="panel-body card-panel-body" style="height:auto !important">
        <ul class="list-group droppable">
            <li class="col-xs-12 list-group-item draggable-card-item draggable-dashboard-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 1</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="amount" style="">$2,000</p>
                    <p class="annual" style="">Annual</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px; clear:both">
                    <div style=""></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item draggable-dashboard-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 2</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="amount" style="">$2,000</p>
                    <p class="annual" style="">Annual</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px; clear:both">
                    <div style=""></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item draggable-dashboard-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 3</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="amount" style="">$2,000</p>
                    <p class="annual" style="">Annual</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px; clear:both">
                    <div style=""></div>
                </div>
            </li>
            <li class="col-xs-12 list-group-item draggable-card-item draggable-dashboard-card-item" >
                <div class="col-xs-8 left-div">
                    <p class="div-title">Don Harper 4</p>
                    <p class="div-desc">Love Communications</p>
                </div>
                <div class="col-xs-4 right-div">
                    <p class="amount" style="">$2,000</p>
                    <p class="annual" style="">Annual</p>
                </div>
                <div class="bottom-div" style="width:100%; padding-left:10px; clear:both">
                    <div style=""></div>
                </div>
            </li>
    
        </ul>
    </div>
</div>