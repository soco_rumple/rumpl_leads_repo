
    	<!-- DRAGGABLE LEAD ITEM MODAL -->
        <div class="modal-dialog">
            <div class="modal-content" style="color:#777 !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <div id="modal-name-title" class="col-md-6">
                        <h3 class="modal-title" style="color:#666; font-family: 'Roboto', sans-serif;"><?php echo $_GET['lead_name']  ?></h3>
                        <h4 style="font-style:italic; color:#888; margin:0; font-size:16px; "><?php echo $_GET['lead_company']  ?></h4>
                    </div>
                    <div id="header-date" class="col-md-6" style="text-align:right">
                        <p style="color:#888; font-size:12px;">Created: <?php echo date('M. d,Y', strtotime($_GET['created_at']));  ?></p>
                    </div>
                    <div class="col-md-12">
                        <hr id="header-line-1" style="margin-top:15px; border-color:#999 !important; " />
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-5">
                                <ul class="list-unstyled" style="font-size:12px; font-weight:bold; margin-bottom: 0;">
                                    <li id="dci-manager"><?php echo $_GET['lead_title']  ?></li>
                                    <li id="dci-email"><?php echo $_GET['lead_email']  ?></li>
                                    <li id="dci-phone"><a href="#" class="rumple-call"><?php echo $_GET['lead_phone']  ?></a></li>
                                    <li id="dci-mobile"><a href="#" class="rumple-call"><?php echo $_GET['lead_mobile'] ?></a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-5">
                                <ul class="list-unstyled" style="font-size:12px; font-weight:bold">
                                    <li id="dci-website"><?php echo $_GET['lead_website']  ?></li>
                                    <li id="dci-address"><?php echo $_GET['lead_address']  ?></li>
                                </ul>
                            </div>
                        </div>
                        
                        <hr  id="header-line-2" style="margin-top:0; border-color:#ddd; " />
                        
                        <textarea class="form-control" id="txtarea-notes" name="txtarea-notes" style="resize: none !important; width: 100% !important;" placeholder="Write a Note" rows = "2"></textarea>
                        <i class="fa fa-paperclip pull-right" style="position:relative; top:-20px; left:-10px; cursor:pointer;" aria-hidden="true"></i>
                        <a id="btn-add-note" href="#" class="btn btn-default btn-xs pull-right btn-dim-gray">Save</a>
                        
                        <div style="clear:both"></div>
                        
                        <hr  id="header-line-3" style="margin-top:0; border-color:#ddd; " />
                        
                        <div class="col-xs-12 col-md-12" style="margin-bottom:10px;">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7 col-md-7 container-border-right">
                                    <div class="dci-head-container container-item-center">
                                        <a id="btn-email-popover" href="#" class="btn btn-xs dci-head-button btn-dim-gray btn-email-popover" data-placement="right" data-toggle="popover" >Email</a>
                                        <a href="#" class="btn btn-xs dci-head-button btn-dim-gray btn-text-popover" data-placement="right" data-toggle="popover" >Text</a>
                                        <a href="#" class="btn btn-xs dci-head-button btn-dim-gray">Call</a>
                                    </div>
                                    <div class="dci-head-container container-item-center ">
                                        <a href="#" class="btn btn-xs dci-head-button btn-dim-gray">Rumple Drop</a>
                                        <a href="#" class="btn btn-xs dci-head-button btn-dim-gray">Visit Website</a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-5 col-md-5">
                                    <div class="dci-head-container container-item-center">
                                        <a id="" href="#" class="btn btn-xs dci-head-button btn-danger btn-reminder-popover" data-placement="left" data-toggle="popover" >Create Reminder</a>
                                        <a href="#" class="btn btn-xs dci-head-button btn-warning btn-schedule-demo-popover" data-placement="left" data-toggle="popover">Schedule Demo</a>
                                    </div>
                                    <div class="dci-head-container container-item-center">
                                        <a id="" href="#" class="btn btn-xs dci-head-button btn-default btn-gray btn-send-lead-popover" data-placement="left" data-toggle="popover" >Send Lead</a>
                                        <a href="#" class="btn btn-xs dci-head-button btn-default btn-gray btn-archive-lead-popover" data-placement="left" data-toggle="popover">Archive Lead</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      
                        <div style="clear:both"></div>
                        
                        <div class="col-md-12">
                            <div class="row">
                                <fieldset class="col-md-12 scheduler-border">
                                    <legend  align="center" class="scheduler-border">Time Line:</legend>
                                    <div id="dci-timeline" class="row"></div>
                                </fieldset>
                            </div>
                        </div>
                        
                        <div style="clear:both"></div>
                    </div>
                </div>
                <div style="clear:both"></div>
                
                <div class="modal-footer" style="text-align:center; background:#b9b9b9; margin-top:10px;">
                    <button class="btn btn-sm btn-default btn-rumple btn-dci-ask-this-week" style="margin:0 1%; width:120px;">Ask This Week</button>
                    <button class="btn btn-sm btn-default btn-rumple btn-dci-ask-next-week" style="margin:0 1%; width:120px;">Ask Next Week</button>
                </div>
            </div>
        </div>
    </div>   
 

    <!-- SEND LEAD POPOVER HTML CONTENT -->
    <div id="send-lead-popover" class="col-md-12" style="display:none;">
    	<div style="max-width:240px; width:240px; height:340px">
            <p style="text-align:center; font-weight:bold; font-size:12px; color:#666">Send to Another Member</p><hr style="border-color:#CCC" />
            <div class="form-group">
                <input name="" type="text" class="form-control input-sm" placeholder="Search Team Members" />
            </div>
            <div class="col-md-12 send-lead-member-list">
                <div class="row"><i class="fa fa-circle fa-2x" style="color:#999" aria-hidden="true"></i> <span class="send-lead-member">Jeremy Garcia</div>
                <div class="row"><i class="fa fa-circle fa-2x" style="color:#999" aria-hidden="true"></i> <span class="send-lead-member">Ryan Burgoyne</div>
                <div class="row"><i class="fa fa-circle fa-2x" style="color:#999" aria-hidden="true"></i> <span class="send-lead-member">Matt Burgoyne</div>
                <div class="row"><i class="fa fa-circle fa-2x" style="color:#999" aria-hidden="true"></i> <span class="send-lead-member">Alex Boye</div>
                <div class="row"><i class="fa fa-circle fa-2x" style="color:#999" aria-hidden="true"></i> <span class="send-lead-member">Drew Barrymore</div>
            </div>
            <div class="col-md-12 popover-footer" style="padding:0 20px 0 0;"> 
                <a href="#" class="btn btn-sm btn-success pull-right" style="margin:10px 0 10px 10px; width:90px">Send</a> 
                <a id="close-btn-send-lead-popover" href="#" class="btn btn-sm btn-default pull-right close btn-dim-gray" style=" margin:10px 0 10px 10px ; width:90px; line-height:1.4; font-weight:normal; font-size:13px;">Cancel</a> 
            </div> 
        </div>
    </div>
    
    <!-- TEXT POPOVER HTML CONTENT -->
    <div id="text-popover" style="display:none; height:auto;"> 
    	<div style=" max-width:350px; width:370px; height:370px;">
            <button id="close-btn-text-popover" type="button" class="close" data-toggle="popover">×</button> 
            <div class="col-md-12"> 
                <ul class="list-inline">
                    <li><i class="fa fa-mobile fa-5x" style="color:#CCC; float:right; margin:0 2%; text-align:right; position:relative; top:10px;" aria-hidden="true" ></i></li>
                    <li><ul class="list-unstyled" style="font-size:12px;color:#666; float:right; text-align:left"> 
                        <li><h4 style="margin:0;">Text Don Harper (don@love.com)</h4></li> 
                        <li><strong>Compose a Rumplified Text To Be sent</strong></li> 
                    </ul></li>
                </ul>
            </div> 
            <div class="form-group"> 
                <div class="col-xs-12 input-group spinner" style="margin:0 auto"> 
                    <input id="popup-input-text" type="text" class="input-sm popup-input-spinner form-control" placeholder="(435)-555-5555" value="" > 
                    <div class="input-group-btn-vertical"> 
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-up"></i></button> 
                        <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-down"></i></button> 
                    </div> 
                </div> 
            </div> 
            <textarea class="form-control" id="" name="" placeholder="Write a Comment" rows="3" style="height:160px; resize: none !important" ></textarea> 
            <div class="col-md-12" style="padding-right:0; padding-top:10px"> 
                <ul class="list-inline">
                    <li class="pull-right"><a href="#" class="btn btn-md btn-success pull-right" style=" width:90px">Send</a></li>
                    <li class="pull-right"><a id="close-btn-reminder-popover" href="#" class="close btn btn-md btn-default pull-right  btn-dim-gray" style=" width:90px; padding: 9px; top:0 !important; margin-top: 0; font-weight:normal; font-size:13px;">Close</a></li>
                </ul>
            </div> 
        </div>
    </div>
    
    <!-- SCHEDDULE DEMO POPOVER HTML CONTENT -->
    <div id="schedule-demo-popover" class="row col-md-12" style="display:none; max-width:620px; width:620px; height:650px; max-height:650px; color:#666 !important"> 
        <div class="row"> 
            <ul class="list-inline" style="text-align:center"> 
                <li><div class="schedule-demo-icon-5x"></div></li> 
                <li> 
                    <ul class="list-unstyled" style="font-size:12px;color:#666; float:right; text-align:left">  
                        <li><h4 style="margin:0;">Schedule Demo</h4></li> 
                        <li><strong>Schedule a Demo with a Specific Person</strong></li> 
                    </ul> 
                </li> 
            </ul> 
        </div> 
        <div class="row" style="margin-bottom:0; padding:0;"> 
            <div class="input-group spinner" style="margin:0 auto; width:400px; float:left"> 
                <input id="popup-input-text" type="text" class="input-sm popup-input-spinner form-control" placeholder="Who Will The Demo Be Schedule With?" value="" > 
                <div class="input-group-btn-vertical"> 
                    <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-up"></i></button> 
                    <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-down"></i></button> 
                </div> 
            </div> 
        </div> 
        <div style="clear:both"></div> 
        <div id="schedule-demo-dates" class="row" style="background:#FFF !important; height: 480px; overflow: scroll; border:thin #CCC solid">
            <div class="col-md-12">
                <h4 style="text-align:center"><i class="fa fa-chevron-left fa-2x sched-prev" aria-hidden="true"></i> <span class="schedule-date-range">10/24/16 - 10/28/16</span> <i class="fa fa-chevron-right fa-2x sched-next" aria-hidden="true"></i></h4>							
                <table id="schedule-demo-schedule-time" class="table table-bordered table-responsive" style="font-size:13px; width:590px; max-width:auto !important">
                    <thead><tr><th>Monday(10/24/16)</th><th>Tuesday(10/24/16)</th><th>Wednesday(10/24/16)</th><th>Thursday(10/24/16)</th><th>Friday(10/24/16)</th></tr></thead>
                    <tbody><tr><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td><td>8:30 AM</td>
                        <tr><td>8:00 AM</td><td>8:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>9:00 AM</td><td>9:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>9:00 AM</td><td>9:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>10:00 AM</td><td>10:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>10:00 AM</td><td>10:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>11:00 AM</td><td>11:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>11:00 AM</td><td>11:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>12:00 AM</td><td>12:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>12:00 AM</td><td>12:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>1:00 AM</td><td>1:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>1:00 AM</td><td>1:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>2:00 AM</td><td>2:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>2:00 AM</td><td>2:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>3:00 AM</td><td>3:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>3:00 AM</td><td>4:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>4:00 AM</td><td>4:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>5:00 AM</td><td>5:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>5:00 AM</td><td>5:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>6:00 AM</td><td>6:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>6:00 AM</td><td>6:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                        <tr><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td><td>7:00 AM</td>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12" style="padding-right:0; padding-top:10px"> 
            <div class="row">
                <a href="#" class="btn btn-sm btn-success pull-right" style="margin:10px 0 10px 10px; width:90px">Set</a> 
                <a id="close-btn-schedule-demo-popover" href="#" class="btn btn-sm btn-default pull-right close btn-dim-gray" style="margin:10px 0 10px 10px ; width:90px; line-height:1.4; font-weight:normal; font-size:13px;">Close</a> 
            </div>
        </div>
	</div>
    
    <!-- ARCHIVE POPOVER HTML CONTENT -->
    <div id="archive-lead-popover" style="display:none; max-width:400px; width:400px; color:#666 !important"> 
        <button id="close-btn-archive-lead-popover" type="button" class="close" data-toggle="popover">×</button> 
        <div class="col-md-12" style="text-align:center; padding-top: 10px;"><h4 style="margin:0;">Are You Sure You Want To Archive This Lead</h4></div> 
        <div class="col-md-12">
            <div class="checkbox" style="font-size:11px;" ><label><input type="checkbox" class="archive-lead-popover-leave-reminder-chkbox" value=""><span class="archive-lead-popover-leave-reminder-txt">Leave Reminder</span></label></div>
            <textarea class="form-control" id="" name="" placeholder="Write a Reminder" rows="2" style="height:120px" ></textarea> 
            <div class="col-sm-12 col-md-12" style="margin-right:0; padding-right:0">
                <ul class="list-inline pull-right" style="margin-right:0">
                    <li>Month Closing</li>
                    <li style="padding-right:0" ><div class="input-group spinner" style="margin:0 auto; position: relative; top: 10px; width:150px;">
                        <input type="text" class="input-sm form-control" placeholder="3 Months" value="" style="width:150px; text-align:center" >
                        <div class="input-group-btn-vertical">
                            <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-up"></i></button>
                            <button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-down"></i></button>
                        </div>
                    </div></li>
                </ul>
            </div>
            <div class="col-md-12" style="padding-right:0; margin: 15px 0;"> 
                <a href="#" class="btn btn-sm btn-success pull-right" style="margin:10px 0 10px 10px; width:90px">Yes</a> 
                <a id="close-btn-archive-lead-popover" href="#" class="btn btn-sm btn-default pull-right close btn-dim-gray" style="margin:10px 0 10px 10px ; width:90px; line-height:1.4; font-weight:normal; font-size:13px;">Cancel</a> 
            </div>
        </div>
    </div>

    <script type="text/javascript" >
		$(document).ready(function() { 
			var tmp = $.fn.popover.Constructor.prototype.show;
			$.fn.popover.Constructor.prototype.show = function () {
				tmp.call(this);
				if (this.options.callback) {
					this.options.callback();
				}
			}
			
			$('.btn-email-popover').popover({
				html : true,
				content: function() {
					return  '<div id="email-popover" class="col-md-12" style="height:auto;margin:-10px -10px 0 -10px; padding-bottom: 10px;"> ' +
							'<div style="width:500px; max-width:500px; height:455px; margin-top: 10px;">' +
								'<button id="close-btn-email-popover" type="button" class="close" data-toggle="popover" style="position:relative; left:10px;"><i class="fa fa-times" aria-hidden="true"></i></button> ' +
								'<div class="col-md-12"> ' +
									'<ul class="list-unstyled" style="font-size:12px;color:#666; float:right; text-align:left"> ' +
										'<li><h4 style="margin:0;">Email Don Harper (don@love.com)</h4></li> ' +
										'<li><strong>Compose a Rumplified Email To Be sent</strong></li> ' +
										'<li><a href="#">Or Select Email From Templates</a></li> ' +
									'</ul>' +
									'<i class="fa fa-envelope fa-5x" style="color:#CCC; float:right; margin:0 2%; text-align:right; position:relative; top:-10px;" aria-hidden="true" ></i> ' +
								'</div>' +
								
								'<input id="popup-input-subject" name="" class="form-control col-md-12 " type="text" placeholder="Enter Subject" /> ' +
								'<div class="form-group" style="margin-bottom:0"> ' +
									'<div class="input-group spinner" style="margin:0 auto"> ' +
										'<input id="popup-input-email" type="text" class="input-sm popup-input-spinner form-control" placeholder="Enter Email" value="" > ' +
										'<div class="input-group-btn-vertical"> ' +
											'<button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-up"></i></button> ' +
											'<button class="btn btn-sm btn-default" type="button"><i class="fa fa-caret-down"></i></button> ' +
										'</div> ' +
									'</div> ' +
								'</div> ' +
								'<div class="summernote" style="height:160px; min-height:160px" ></div> ' +
								'<div class="col-md-12 popover-footer" style="padding-right:0; padding-top:10px"> ' +
									'<ul class="list-inline">' +
										'<li class="pull-right"><a href="#" class="btn btn-md btn-success pull-right" style=" width:90px">Send</a></li>' +
										'<li class="pull-right"><a id="close-btn-reminder-popover" href="#" class="close btn btn-md btn-default pull-right  btn-dim-gray" style=" width:90px; padding: 9px; top:0 !important; margin-top: 0; font-weight:normal; font-size:13px;">Close</a></li>' +
									'</ul>' +
								'</div> ' +
							'</div>' +
						'</div><script>$(".summernote").summernote({ height: 160 })<\/script>';
				},
				callback: function() { 
					$('.popover').css({ "width": "540px", "max-width": "540px" });
					$('.note-editing-area').css({ "min-height": "160px" });
				}
			});
			
			//close and remove popover when clicking outside
			$('body').on('click', function (e) {
				if ($(e.target).data('toggle') !== 'popover'
					&& $(e.target).parents('.popover.in').length === 0) { 
					$(".popover, .note-editor").remove();
				}
			});
			
			//close and remove popover when clicking close button
			$(document).on('click', '.popover .close' , function(){
				$('.' + $(this).attr('id').replace('close-', '')).trigger('click');
				$(".popover, .note-editor").remove();
			});
				
			//send lead click button
			$('.btn-send-lead-popover').popover({
				html : true,
				content: function() {
					return $('#send-lead-popover').html();
				},
				callback: function() {
					$('.popover').css({ "width": "270px", "max-width": "270px" });
				}
			});
			
			$('.btn-text-popover').popover({
				html : true, 
				content: function() {
					return $('#text-popover').html();
				}
			});
			
			$('.btn-reminder-popover').popover({
				html : true, 
				content: function() {
					return '<div id="reminder-popover" class="col-md-12" style=" max-width:435px; width:400px; padding:10px; height:455px; max-height:455px; color:#666 !important"> ' +
								'<div class="row"> ' +
									'<ul class="list-inline"> ' +
										'<li><div class="reminder-icon-5x"></div></li> ' +
										'<li> ' +
											'<ul class="list-unstyled" style="font-size:12px;color:#666; float:right; text-align:left">  ' +
												'<li><h4 style="margin:0;">Create Reminder</h4></li> ' +
												'<li><strong>Create a reminder to call, text or email Don Harper</strong></li> ' +
											'</ul> ' +
										'</li> ' +
									'</ul> ' +
								'</div> ' +
								'<div class="form-group"> ' +
									'<div class="input-group" style="margin:0 auto; width:100%">' + 
										'<input id="reminder-input-text" type="text" class="input-sm form-control" placeholder="What do you want to be reminded to do?" value="" > ' +
									'</div> ' +
								'</div> ' +
								'<div style="overflow:hidden;">' +
									'<div class="form-group" style="background: #fff none repeat scroll 0 0; border: thin solid #ccc; border-radius: 4px; margin-bottom: 0; margin-top: 0; padding-top: 10px;">' +
										'<div class="row">' +
											'<div class="col-md-12">' +
												'<div id="dt-reminder"></div>' +
											'</div>' +
										'</div>' +
									'</div>' +
								'</div>' +
								'<div style="clear:both;"></div>' +
								'<div class="col-md-12" style="padding-top:10px; padding-right:0"> ' +
									'<ul class="list-inline">' +
										'<li class="pull-right"><a href="#" class="btn btn-md btn-success pull-right" style=" width:90px">Send</a></li>' +
										'<li class="pull-right"><a id="close-btn-reminder-popover" href="#" class="close btn btn-md btn-default pull-right  btn-dim-gray" style=" width:90px; padding: 9px; top:0 !important; margin-top: 0; font-weight:normal; font-size:13px;">Close</a></li>' +
									'</ul>' +
								'</div>' +
							'</div></div><script>$("#dt-reminder").datetimepicker({ inline: true, }); $(".bootstrap-datetimepicker-widget .new, .bootstrap-datetimepicker-widget .old").html("");<\/script>';
				},
				callback: function() {
					$('.popover').css({ "max-width": "435px" });
				},
				placement: function (context, source) {
					var position = $(source).position();
					if (position.left < 7) {
						return "left";
					}
					if (position.left < 46) {
						return "left";
					}
					return "top";
				}
			});
			
			$('.btn-schedule-demo-popover').popover({
				html : true, 
				content: function() {
					return $('#schedule-demo-popover').html();
				},
				callback: function() {
					$('.popover').css({ "padding": "10px" });
				}
			})
			
			$('.btn-archive-lead-popover').popover({
				html : true, 
				content: function() {
					return $('#archive-lead-popover').html();
				},
				callback: function() {
					$('.popover').css({ "max-width": "550px" });
				}
			});
			$('#dci-timeline').slimScroll({ wheelStep: 20, start: 'top', alwaysVisible: true });
			
			$('#txtarea-notes').slimScroll({ "height": "80px", wheelStep: 5 }).mouseover(function() {
				$(this).next('.slimScrollBar').css('opacity', 0.4);
			});
			
		})
	</script>
    
                