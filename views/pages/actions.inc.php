	
    <!-- Stats tables -->
    <div class="table-container">
        <?php include_once($view_path."pages/new-leads.inc.php"); ?>
    </div>
    <div class="table-container">
        <?php include_once($view_path."pages/left-message.inc.php"); ?>
    </div>
    <div class=" table-container">
        <?php include_once($view_path."pages/send-email.inc.php"); ?>
    </div>
    <div id="add-table" class="add-table-container">
        <a id="add-link" href="#">Add a Table</a>
        <div id="select-container" class="form-group" style="">
            <div class="btn-group">
                <button id="option-btn" type="button" class="btn btn-default btn-tbl-colors dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;" ><span class="caret"></span></button>
                <ul id="option-colors" class="dropdown-menu scrollable-menu" role="menu" style="">
                    <li><div id="17975f"></div></li>
                    <li><div id="fd5151"></div></li>
                    <li><div id="5c74f8"></div></li>
                    <li><div id="51dcfd"></div></li>
                    <li><div id="fdb451"></div></li>
                    <li><div id="fd7f51"></div></li>
                </ul>
            </div>
        </div>
    </div>
    
    <!-- Dashboard tables -->
    <div class="table-container dashboard-table-container">
        <?php include_once($view_path."pages/dashboard-new-leads.inc.php"); ?>
    </div>
    <div class="table-container dashboard-table-container">
        <?php include_once($view_path."pages/dashboard-new-leads2.inc.php"); ?>
    </div>
    <div class="table-container dashboard-table-container">
        <?php include_once($view_path."pages/dashboard-new-leads3.inc.php"); ?>
    </div>