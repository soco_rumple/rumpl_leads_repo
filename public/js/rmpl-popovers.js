
	//event raised when clicking or adding new table
	$(document).on('click', '#add-link', function() {
		$('#add-table').prepend('<div class="form-group"><input type="text" class="form-control" id="new-card-input" placeholder = "Add a table"></div>' +
								'<button class="btn btn-sm btn-default pull-right" id="new-card-btn" style="margin:0 0.1%; width:80px; background:#45ac7f; color:#FFF; border-radius:3px; ">Save</button>');
		$('#select-container, .btn-group #option-btn').css("display", "inline-block");
		$('#add-link').css("display", "none");
		$("#new-card-input").focus();
		
		return false;
	})
	