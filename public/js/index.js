
	var element_zindex = 99;
	var previousDrop;
	var modActionsWidth;
	var tblSelectedBgColor = "#17975f";
	var wrapperDefaultHeight = $('#wrapper').height();
	var tblContainerWidth = $('.table-container').width();
	var tblWidthMargin = (parseInt($('.table-container').css('margin-right')) + parseInt($('.table-container').css('margin-left')));
	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec" ];
	var date = new Date();
	var hour = date.getHours() - (date.getHours() >= 12 ? 12 : 0);
	var period = date.getHours() >= 12 ? 'PM' : 'AM';

	var droppableOptions = {
		containment: ".container-inner", 
		cursor: "move",
		snapTolerance: 1,
		drop: function(event, ui) {
			//switch table cards when dropping
			if (ui.draggable.hasClass('card-panel')) {
				var dropped = ui.draggable;
				var droppedOn = this;
				if ($(droppedOn).children().length > 0) {
					$(droppedOn).children().detach().prependTo($(previousDrop));
				}
			
				$(dropped).detach().prependTo($(droppedOn)).css({ top: 0, left: 0 });
			}
			$(this).css("background", "transparent");
			moveTables(ui.draggable, $(previousDrop));
		},
		over: function (event, ui) { 
			var hoveredElement = $(this);
			if($(this).children(".card-panel").length > 0) {
				$('.table-container').css("background", "transparent");
			} else {
				hoveredElement.css("background", "#e7f8f1");
			}
		}
	}
	
	var sortableOptions = {
		containment: ".container-outer", 
		connectWith: 'ul',
		cursor: "move",
		helper: 'clone',
		appendTo: 'body',
		helper: 'original',
		forceHelperSize: true,
        scroll: true,
		zIndex: 10000,
		start: function(event, ui) {
			$('#'+$(this).parent().parent().find('.card-panel-body').attr('id')).css({"z-index": element_zindex++, "cursor": "move"}); //, 
			
			$(ui.item).css({"z-index": element_zindex, "cursor": "move !important"});
			$(ui.item).parent().parent().parent().parent().css("z-index", element_zindex++);
			$(ui.item).parent().css("height", $(ui.item).parent().parent().height());
			
			$('.slimScrollBar').css('display', 'none');
		},
		stop: function(event, ui) {
			$('#'+$(this).parent().parent().find('.card-panel-body').attr('id')).css({"cursor": "default", "position": "relative"});
			$(ui.item).parent().parent().parent().parent().css("z-index", 0);
			console.log($(ui.item).parent().parent().parent().parent());
			$('.slimScrollBar').css('display', 'none');
		},
		helper: function(event, ui){
			//console.log(ui);
			 $('.container-inner').append('<div class="col-xs-12 list-group-item draggable-card-item" id="clone" >' + ui.html() + '</div>');
			 $("#clone").hide();
			 setTimeout(function(){
				 $('#clone').appendTo('body');
				 $("#clone").show();
			 },1);
			 
			 return $("#clone");
		}
	}
	
	var draggableOptions = {
		containment: ".container-outer", 
		cancel: '.card-panel-body',
		stack: ".card-panel",
		snap: ".table-container",
        snapTolerance: 1,
		scrollSpeed: 25,
		scrollSensitivity: 100,
		revert: true ,
		start: function(event, ui) {
			$(this).css({"z-index": element_zindex++, " overflow-y": "hidden"});
			$(this).children('.panel-heading').css("cursor", "move");
			$('.table-container').css("height", ui.helper.height());
			
			//hide table options where dragging
			$('.dropdown').removeClass("open");
			
			previousDrop = $(this).parent(); // get previous drop
		},
		stop: function() {
			//$(".card-panel").css("z-index", 0);
			$(this).children('.panel-heading').css("cursor", "default");
		}
	}
	
	//set height and widths for scolling
	onResize = function() {
		var visibleTableWidth = 0;
		$(".table-container:visible").each(function() {
			visibleTableWidth += $(this).width() + parseInt($(this).css('margin-right')) + parseInt($(this).css('margin-left'));
		})
		
		//$('.container-outer').css({"max-width": $(window).width()});

		$('.container-outer').height($('#wrapper').height() + 9 );
		
		if($(window).width() < 769) {
			$('.container-outer').height($('#wrapper').height() - 105 );
		} else if($(window).width() < 769) {
			$('.container-outer').height($('#wrapper').height() - 90 );
		} else if($(window).width() < 992) {
			$('.container-outer').height($('#wrapper').height() - (parseInt($('#top-nav').height()) + 5) );
		} 
		
		if($('.table-container').is(":visible")) {
			$('.container-inner').css({"width": (visibleTableWidth + $('.table-container').width()) + 50 });
		} else {
			$('.container-inner, .container-outer').css({"width": "100%" });
		}
		
		$('#left-nav').height($('#left-nav').height() + 66);
		
		$('.card-panel-body').css({"max-height": $('.container-outer').height() - 85 });
		
		$('#reminder-popover').parent().parent().css({ "width": "435px", "max-width": "435px" });
		$('#send-lead-popover').parent().parent().css({ "width": "300px", "max-width": "300px" });
		
		$('.timeline-row').each(function(i, obj) {
			$($($(this).children()[0]).children()[2]).height($(this).height());
		});
		

	}
	$(document).ready(onResize);
	$(window).bind('resize', onResize);
	
	$(".card-panel").draggable(draggableOptions); //make card panel stack draggable

	$('.table-container').droppable(droppableOptions); //make card container droppable
	
	$('.card-panel-body ul').sortable(sortableOptions);  //sort ul li elements
	
	//$('.panel-body').css({"max-height": $('.container-outer').height()});
	
	$(document).ready(function() {
		$('.card-panel-body').css({"overflow": "hidden"});
		$('.card-panel-body').slimScroll({ opacity: 0 }).mouseover(function() {
			$(this).next('.slimScrollBar').css('opacity', 0.4);
		});
	});

	//$('.container-outer').slimScroll();
	
	//Show new lead modal
	$('#btn-new-lead').on('click',function(){
		$('#lead-modal').modal();
		$($('#wrapper').height($('#wrapper').height() + 170))
		$('.modal-backdrop').appendTo('#wrapper');
	});
	
	//show upload lead sheet modal
	$('#btn-upload-lead-sheet').on('click',function(){
		$('#upload-sheet-modal').modal();
		$($('#wrapper').height($('#wrapper').height() + 170))
		$('.modal-backdrop').appendTo('#wrapper');
	});
	
	$('.table-options').on('click', function() {
		$(this).parent().parent().css("z-index", element_zindex++); //bring forward table options
	})
		
	if($('#btn-stats').hasClass('active')) {
		$('.table-container, .add-table-container').css({"display": "inline-block"});
		$('.dashboard-table-container').css({"display": "none"});
	} else if($('#btn-dashboard').hasClass('active')) {
		$('.table-container, .add-table-container').css({"display": "none"});
		$('.dashboard-table-container').css({"display": "inline-block"});
	}
	
	$('#btn-card-view button').click(function() {
		$(this).addClass('active').siblings().removeClass('active');
		
		if($(this).attr('id') == "btn-stats") {
			$('.table-container, .add-table-container').css({"display": "inline-block"});
			$('.dashboard-table-container').css({"display": "none"});
			
		} else if($(this).attr('id') == "btn-dashboard") {
			$('.table-container, .add-table-container').css({"display": "none"});
			$('.dashboard-table-container').css({"display": "inline-block"});
		}
	});
		
	$(document).on('dp.update', function (e) {
		$(".bootstrap-datetimepicker-widget").find(".old, .new").html("");
	});
	
	$(document).on('dp.change', function (e) {
		$(".bootstrap-datetimepicker-widget").find(".old, .new").html("");
	});
	
	$(document).on('click', '.dci-head-button' , function(){
		$('.dci-head-button').not(this).popover('hide');
	});
	
	$('a.rumple-call').on('click', function(){
		var left = Math.round((screen.width / 2)-(600 / 2));
		var top = Math.round((screen.height / 2)-(400 / 2));
		window.open("http://localhost/rumple-call", "", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=' + 600 
				+ ', height=' + 600 + ', top=' + top + ', left=' + left);
	});
	
	//change dropdown menu arrow when clicking
	$(document).on('click', '#leads, #new-customer', function() {
		if($(this).find('i').hasClass('fa-angle-left')) {
			$(this).find('i').removeClass('fa-angle-left');
			$(this).find('i').addClass('fa-angle-down');
			$(this).next().next().css({'border-top': '0.2em solid #8bcbaf'});
		} else {
			$(this).find('i').removeClass('fa-angle-down');
			$(this).find('i').addClass('fa-angle-left');
			$(this).next().next().fadeIn('slow').css({'border-top': '0'});
		}
		return false;
	});
	
	//show profile modal
	$('#btn-schedule-modal').on('click',function(){
		//$('.modal').not($(this)).each(function () { $(this).modal('hide'); }); //hide opened modal/s
		
		$('#schedule-modal').modal();
		$('.modal-backdrop').appendTo('#cards');
	});
	
	//show profile modal
	$('#btn-profile-modal').on('click',function(){
		//$('.modal').not($(this)).each(function () { $(this).modal('hide'); }); //hide opened modal/s
		
		$('#profileModal').modal();
		$('.modal-backdrop').appendTo('#cards');
	});
	
	//show reminders modal
	$('#reminders').on('click',function(){
		//$('.modal').not($(this)).each(function () { $(this).modal('hide'); });
		
		$('#reminders-modal').modal();
		$('.modal-backdrop').appendTo('#cards');
	});
	
	$('#profile-outer-container').slimScroll({ "height": "400px", railVisible: true , wheelStep: 5});
	
	$('#reminders-outer-container').slimScroll({ "height": "400px", railVisible: true , wheelStep: 5});
	
	//table options assign color to dropdown btn
	$(document).on('click', '#option-colors li div', function() {
		var selectedColor = "#" + $(this).attr('id');
		
		if($($(this).parents().eq(2).children()[0]).hasClass('btn-tbl-colors')) {
			tblSelectedBgColor = "#" + $(this).attr('id');
			$($(this).parents().eq(2).children()[0]).css({"background": selectedColor, "border-color": selectedColor});
		} else {
			$($(this).parents().eq(2).children()[0]).css({"background": selectedColor, "border-color": selectedColor});
			$(this).parents().eq(8).css({"border-color": selectedColor});
			$($(this).parents().eq(8).children()[0]).css({"background": selectedColor, "border-color": selectedColor});
		}
	})
	
    //Prevent table option from being hidden when clicking inside element
	$('#drp-tbl-opt.dropdown').on({ "click": function(event) {
		if ($(event.target).closest('.dropdown-toggle').length) {
		  	$(this).data('closable', false);
		} else {
		  $(this).data('closable', false);
		}
	}, "hide.bs.dropdown": function(event) {
			hide = $(this).data('closable');
			$(this).data('closable', true);
			return hide;
		}
	});

	//Assign value background and border color when clicking table option when recolors table
	$(document).on('click', '#drp-clr-opt', function(e) {
		e.stopPropagation();
        $(this).removeClass('open');
    });
	//close table option
	$(document).on('click', '.close', function(e) {
		e.stopPropagation();
		$(this).parent().parent().removeClass('open');
	});	
	
	$(document).on('click', '.btn-table-options-dropdown', function(e) {
		e.stopPropagation();
		$(this).parent().data('closable', false);
		$(this).parents().eq(5).addClass('open');
	});

	
	

	

	

	
	




















