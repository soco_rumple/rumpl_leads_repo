
	//show weekly history
	$(document).on('click', '#weekly-history', function() {
		$('.non-action-leads').hide();
		$('.container-head, .table-container, .add-table-container').hide();
		$('.container-outer').height($('#wrapper').height()  + (parseInt($('.container-head').height()) + 20));
		$('.container-inner').css({ "width": "100%" });
		$('#weekly-history-container').css({ "display": "block" }).animate({'top': '20'}, { duration: 1, complete: function() { $(this).animate({top: 0}, { duration: 100}); }})

		return false;
	})
	$('#weekly-history-count').flipcountdown({ tick: function() { return "120620"; } });
	
	//re-asks
	$(document).on('click', '#re-ask', function() {
		$('.non-action-leads').hide();
		$('.container-head, .table-container, .add-table-container').hide();
		$('.container-outer').height($('#wrapper').height()  + (parseInt($('.container-head').height()) + 20));
		$('.container-inner').css({ "width": "100%" });
		$('#re-ask-container').css({ "display": "block" }).animate({'top': '20'}, { duration: 1, complete: function() { $(this).animate({top: 0}, { duration: 100}); }})

		return false;
	})
	$('#re-ask-count').flipcountdown({ tick: function() { return "120620"; } });
	
	//last week
	$(document).on('click', '#last-week', function() {
		$('.non-action-leads').hide();
		$('.container-head, .table-container, .add-table-container').hide();
		$('.container-outer').height($('#wrapper').height()  + (parseInt($('.container-head').height()) + 20));
		$('.container-inner').css({ "width": "100%" });
		$('#last-week-container').css({ "display": "block" }).animate({'top': '20'}, { duration: 1, complete: function() { $(this).animate({top: 0}, { duration: 100}); }})

		return false;
	})
	$('#last-week-count').flipcountdown({ tick: function() { return "120620"; } });

	//show this week
	$(document).on('click', '#this-week', function() {
		if($(this).hasClass('active')) {
			return false;
		}
		$('.non-action-leads').hide();
		$('.container-head, .table-container, .add-table-container').hide();
		$('.container-outer').height($('#wrapper').height()  + (parseInt($('.container-head').height()) + 20));
		$('.container-inner').css({ "width": "100%" });
	
		$('#this-week-container').css({ "display": "block" }).animate({'top': '20'}, { duration: 1, complete: function() { $(this).animate({top: 0}, { duration: 100}); }})
				
		return false;
	})
	$('#this-week-count').flipcountdown({ tick: function() { return "120620"; } });
	
	//next-week
	$(document).on('click', '#next-week', function() {
		$('.non-action-leads').hide();
		$('.container-head, .table-container, .add-table-container').hide();
		$('.container-outer').height($('#wrapper').height()  + (parseInt($('.container-head').height() + 20)));
		$('.container-inner').css({ "width": "100%" });
		$('#next-week-container').css({ "display": "block" }).animate({'top': '20'}, { duration: 1, complete: function() { $(this).animate({top: 0}, { duration: 100}); }})
		
		return false;
	})
	$('#next-week-count').flipcountdown({ tick: function() { return "120620"; } });
	
	//actions
	$(document).on('click', '#actions', function() {
		if($($(this).children()[0]).hasClass('active')) {
			return false;
		}
		$('#wrapper').height(wrapperDefaultHeight); //reset wrappers height
		$('.container-inner').css({ "width": modActionsWidth});
		$('.container-outer').height(parseInt($('#wrapper').height()) + 9);
		$('.dashboard-table-container').hide();
	
		return false;
	})
	
	$(document).on('click', '.dropdown-menu a', function() {
		$(this).closest('.dropdown').find('input.input-select-lead').val($(this).text());
	});
	
	//NOTE: TEMPORARY DISPLAY FOR ACTIONS
	$(document).ready(function(e) {
        $("#lead-subitems li a#lead-action").trigger('click');
    });
	
	//create new table
	$(document).on('click', '#new-card-btn', function() {
		$('#myPleaseWait').modal('show');
		$('.modal-backdrop').appendTo('#cards');
		
		$.ajax({
			type: "POST",
			url: "/createtable",
			data: { "tablename": $('#new-card-input').val(), "tablebgcolor": tblSelectedBgColor }
		}).done(function(data) {
			//increment width size of container-inner
			if((parseInt($('.card-panel').length + 2) * (tblContainerWidth + tblWidthMargin))  > $('.container-inner').width()) {
				//console.log('Needs to resize now.. ');
				$('.container-inner').css("width", "+=" + (tblContainerWidth + tblWidthMargin));
				modActionsWidth = $('.container-inner').width();
			}
	
			//dynamically add new card table
			var newTableContainer = $('<div class="table-container">' +
				'<div id="'+ data['ret_id'] +'" class="panel panel-default card-panel" style="border-color:'+ tblSelectedBgColor +' !important">' +
					'<div id="new-panel-heading" class="panel-heading stats-panel-heading" style="text-align:right; height:40px; background:'+ tblSelectedBgColor +' !important;  border-color:'+ tblSelectedBgColor +' !important"> <span class="pull-left">' + $("input[id='new-card-input']").val() +'</span> ' +
				'</div>' +
				'<div id="drp-tbl-opt" class="col-md-12 dropdown" style="top:-30px;height: 2px;">' +
				'<i class="icon-ellipsis-horizontal icon-large table-options pull-right" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>' +
				'<ul class="dropdown-menu card-dropdown-menu" aria-labelledby="">' +
					'<div class="arrow-up"></div>' +
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position:relative; top:-5px; left:-10px;" ><span aria-hidden="true">&times;</span></button>' +
					'<li><h4 style="color:#333; text-align:center; font-size:14px;">Table Options</h4></li>' +
					'<li role="separator" class="divider" style="position:relative; left:5px"></li>' +
					'<li><a href="#">Send Email To Table</a></li>' +
					'<li><a href="#">Send Text To Table</a></li>' +
					'<li><a href="#">Send Drop To Table</a></li>' +
					'<li role="separator" class="divider"></li>' +
					'<li><a href="#">Move Leads To Another Table</a></li>' +
					'<li><a href="#">Move Leads To Another User</a></li>' +
					'<li role="separator" class="divider"></li>' +
					'<li>' +
						'<ul class="list-inline" style="padding:0 20px;">' +
							'<li class="pull-left"><span class="gray-text">Recolor Table</a></li>' +
							'<li class="pull-right">' +
								'<div id="drp-clr-opt" class="dropdown">' +
									'<button id="dd-colors" type="button" class="btn btn-default dropdown-toggle btn-table-options-dropdown" style="background: '+ tblSelectedBgColor +'; border-color: '+ tblSelectedBgColor +'" data-toggle="dropdown" ><span class="caret"></span></button>' +
									'<ul id="option-colors" class="dropdown-menu scrollable-menu " role="menu">' +
										'<li><div id="17975f"></div></li>' +
										'<li><div id="fd5151"></div></li>' +
										'<li><div id="5c74f8"></div></li>' +
										'<li><div id="51dcfd"></div></li>' +
										'<li><div id="fdb451"></div></li>' +
										'<li><div id="fd7f51"></div></li>' +
									'</ul>' +
								'</div>' +
							'</li>' +
						'</ul>' +
						
					'</li>' +
					'<li><a href="#">Archieve Table</a></li>' +
				'</ul>' +
			'</div>' +
			'<div style="clear:both"></div>' +
				'<div class="panel-body card-panel-body">' +
					'<ul id="new-card-list" class="list-group droppable "></ul>' +
				'</div>' +
			'</div>').insertBefore(".add-table-container").css({"opacity":"0.4"}).animate({ opacity: '0.9', }, 1500 );;
			
			newTableContainer.droppable(droppableOptions); 
			
			$('ul#new-card-list').sortable(sortableOptions); 
			
			$('.panel').draggable(draggableOptions); 
			
			$('.card-panel-body').slimScroll({ wheelStep: 5 });
		
			$('.container-outer').animate({ scrollLeft: $('.container-outer')[0].scrollWidth + 'px' }, 800); //scroll at the end of the container
		
			//show add table link
			$('#add-table .form-group, #add-table button').css("display", "none");
			$('#add-table a, .btn-group #option-btn').css("display", "inline-block");
			
			setTimeout(function() { $('#myPleaseWait').modal('hide'); }, 1000);
		});
	})
	
	//When Leads action is clicked
	$("#lead-subitems li a").on('click', function() {
		if($(this).hasClass('active')) {
			return false;
		} else {
			$("#lead-subitems li a").parent().children().removeClass('active');
			$(this).addClass('active');
		}
		
		switch($(this).attr('id')) {
			case "lead-action":
				$('.non-action-leads').hide();
				$('.container-head, .table-container, .add-table-container').hide().fadeIn(50);
				showLeadTables();
				
			break;
		default:
			$('.table-container, .add-table-container').remove();
		
			break;	
		}
	});
	
	function showLeadTables() {
		$('.container-inner').css({ "width": "3000px", "min-width": "400px" });
		
		$.ajax({
			type: "GET",
			url: "/gettables",
		}).done(function(data) {
			var newTable = null;
			for(var y = 0; y < data['tables'].length; y++) { 
				newTable = '<div index="TABLE_INDEX" position="TABLE_ORDINAL_POSITION" class="table-container">' +
					'<div id="TABLE_ID" class="panel panel-default card-panel" style="border-color: TABLE_BORDER_COLOR !important">' +
						'<div class="panel-heading stats-panel-heading" style="height:40px; background-color:TABLE_BG_COLOR !important; border-color: TABLE_BG_COLOR !important">' +
							'<span class="pull-left">TABLE_TITLE</span> ' +
						'</div>' +
						'<div id="drp-tbl-opt" class="col-md-12 dropdown" style="top:-30px;height: 2px; float:right">' +
							'<i class="icon-ellipsis-horizontal icon-large table-options pull-right" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>' +
							'<ul class="dropdown-menu card-dropdown-menu" aria-labelledby="">' +
								'<div class="arrow-up"></div>' +
								'<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position:relative; top:-5px; left:-10px;" ><span aria-hidden="true">&times;</span></button>' +
								'<li><h4 style="color:#333; text-align:center; font-size:14px;">Table Options</h4></li>' +
								'<li role="separator" class="divider" style="position:relative; left:5px"></li>' +
								'<li><a href="#">Send Email To Table</a></li>' +
								'<li><a href="#">Send Text To Table</a></li>' +
								'<li><a href="#">Send Drop To Table</a></li>' +
								'<li role="separator" class="divider"></li>' +
								'<li><a href="#">Move Leads To Another Table</a></li>' +
								'<li><a href="#">Move Leads To Another User</a></li>' +
								'<li role="separator" class="divider"></li>' +
								'<li>' +
									'<ul class="list-inline" style="padding:0 20px;">' +
										'<li class="pull-left"><span class="gray-text">Recolor Table</a></li>' +
										'<li class="pull-right">' +
											'<div id="drp-clr-opt" class="dropdown">' +
												'<button id="" type="button" class="btn btn-default dropdown-toggle btn-table-options-dropdown" data-toggle="dropdown" ><span class="caret"></span></button>' +
												'<ul id="option-colors" class="dropdown-menu scrollable-menu " role="menu">' +
													'<li><div id="17975f"></div></li>' +
													'<li><div id="fd5151"></div></li>' +
													'<li><div id="5c74f8"></div></li>' +
													'<li><div id="51dcfd"></div></li>' +
													'<li><div id="fdb451"></div></li>' +
													'<li><div id="fd7f51"></div></li>' +
												'</ul>' +
											'</div>' +
										'</li>' +
									'</ul>' +
								'</li>' +
								'<li><a href="#">Archieve Table</a></li>' +
							'</ul>' +
						'</div>' +
						'<div style="clear:both"></div>' +
						'<div class="panel-body card-panel-body"><ul class="list-group droppable">';
						
						if(typeof (data['tables'][y]) !== 'undefined') {
							if(typeof (data['tables'][y]['leads']) !== 'undefined') {
					
								for(var i = 0; i < data['tables'][y]['leads'].length; i++) { 
									var d = new Date(data['tables'][y]['leads'][i].created_at);
									
									 var newLead = '' +
										'<li id='+ data['tables'][y]['leads'][i].id +' class="col-xs-12 list-group-item draggable-card-item" >' +
											'<div class="col-xs-8 left-div">' +
												'<p class="div-title">'+ data['tables'][y]['leads'][i].lead_name +'</p>' +
												'<p class="div-desc">'+ data['tables'][y]['leads'][i].lead_company +'</p>' +
											'</div>' +
											'<div class="col-xs-4 right-div">' +
												'<p class="month-day" >'+ monthNames[d.getMonth()] +' '+ d.getDate() +'</p>' +
												'<p class="year">'+ d.getFullYear() +'</p>' +
											'</div>' +
											'<div class="bottom-div" style="width:100%; padding-left:10px;">' +
												'<div></div>' +
												'<div></div>' +
												'<div></div>' +
												'<div></div>' +
												'<div></div>' +
												'<div></div>' +
											'</div>' +
										'</li>' +
									'';
									newTable += newLead;
								}
							}
						}
							
				newTable += '</ul></div>' +
					'</div>' +
				'</div>';
				$('.container-inner').append($(newTable
													.replace("TABLE_TITLE", data['tables'][y].table_name)
													.replace("TABLE_BG_COLOR", data['tables'][y].table_color)
													.replace("TABLE_BORDER_COLOR", data['tables'][y].table_color)
													.replace("TABLE_ID", data['tables'][y].id)
													.replace("TABLE_INDEX", data['tables'][y].id)
													.replace("TABLE_ORDINAL_POSITION", data['tables'][y].table_ordinal_position)
												).hide().fadeIn(600)
											);
			}
			//reinit draggables and droppables
			newTable += '<script type="text/javascript" src="../../public/js/index.js"></script>';
			
			$('.container-inner').append('<div id="add-table" class="add-table-container"> <a id="add-link" href="#">Add a Table</a> <div id="select-container" class="form-group" style=""> <div class="btn-group"> <button id="option-btn" type="button" class="btn btn-default btn-tbl-colors dropdown-toggle" data-toggle="dropdown" style="border-radius:2px;" ><span class="caret"></span></button> <ul id="option-colors" class="dropdown-menu scrollable-menu" role="menu" style=""> <li><div id="17975f"></div></li> <li><div id="fd5151"></div></li> <li><div id="5c74f8"></div></li> <li><div id="51dcfd"></div></li> <li><div id="fdb451"></div></li> <li><div id="fd7f51"></div></li> </ul> </div> </div> </div><script type="text/javascript" src="../../public/js/index.js"></script>');
		});	
	}
	

	function moveTables (draggedElement, previousDropped) {
		$.ajax({
			type: "POST",
			url: "/updateOrdinalPosition",
			data: { "dragged_element_id": draggedElement.attr('id'),
					"dragged_element_position": draggedElement.parent().attr('position'), 
					"previous_dropped_id": $(previousDropped.children()[0]).attr('id'),
					"previous_dropped_position": previousDropped.attr('position')
				  }
		}).done(function(data) {});
		
	}
	
	$(document).on('click', '.stats-panel-heading', function() {
		$('a#btn-save-lead').attr('tableid', $(this).parent().attr('id'));
		$('.card-panel').removeClass('one-edge-shadow');
		$(this).parent().addClass('one-edge-shadow');
		$('#btn-new-lead').prop( "disabled", false );
	})	

	//insert new lead
	$(document).on('click', '#btn-save-lead', function() {
		var el = '#' + $(this).attr('tableid') + ' .card-panel-body ul';
		
		$.ajax({
			type: "POST",
			url: "/createlead",
			data: { "lead_name": $('#lead-name').val(),
					"lead_company": $('#lead-company').val(),
					"lead_title": $('#lead-title').val(),
					"lead_email": $('#lead-email').val(),
					"lead_phone": $('#lead-phone').val(),
					"lead_website": $('#lead-website').val(),
					"lead_address": $('#lead-address').val(),
					"table_id": $(this).attr('tableid')
				  }
		}).done(function(data) {
			var d = new Date();
			var new_lead = '' +
				'<li id="'+ data["ret_id"] +'" class="col-xs-12 list-group-item draggable-card-item" >' +
					'<div class="col-xs-8 left-div">' +
						'<p class="div-title">'+ $('#lead-name').val() +'</p>' +
						'<p class="div-desc">'+ $('#lead-company').val() +'</p>' +
					'</div>' +
					'<div class="col-xs-4 right-div">' +
						'<p class="month-day" >'+ monthNames[d.getMonth()] +' 26</p>' +
						'<p class="year">'+ d.getFullYear() +'</p>' +
					'</div>' +
					'<div class="bottom-div" style="width:100%; padding-left:10px;">' +
						'<div></div>' +
						'<div></div>' +
						'<div></div>' +
						'<div></div>' +
						'<div></div>' +
						'<div></div>' +
					'</div>' +
				'</li>';
			
			//reinit draggables and droppables
			new_lead += '<script type="text/javascript" src="../../public/js/index.js"></script>';
			
			$(el).prepend(new_lead);
			
			//clear input texts
			$('#lead-name').val('');
			$('#lead-company').val('');
			$('#lead-title').val('');
			$('#lead-email').val('');
			$('#lead-phone').val('');
			$('#lead-website').val('');
			$('#lead-address').val('');
			
			$('#lead-modal').modal('hide');
		});
		
	})
	
	//show draggable card item modal
	$(document).on('click', '.draggable-card-item', function(){
		$('#draggable-card-item-modal').modal();
		
		$.ajax({
			type: "POST",
			url: "/getlead",
			data: { 'lead_id': $(this).attr('id') }
		}).done(function(data) {
			var lead_data = data['lead_detail'][0];
			$.get('../../views/pages/lead-popup-detail.inc.php', { 
					'lead_name': lead_data.lead_name, 'lead_company': lead_data.lead_company, 'lead_title': lead_data.lead_title, 'lead_email': lead_data.lead_email,
					'lead_phone': lead_data.lead_phone, 'lead_mobile': lead_data.lead_mobile, 'lead_website': lead_data.lead_website, 'lead_address': lead_data.lead_address,
					'created_at': lead_data.created_at
				}, function(raw_html){
				$('#draggable-card-item-modal').html(raw_html);
			})
			
			//show lead timelines
			showLeadTimeline();
		});
		$($('#wrapper').height($('#wrapper').height() + 170))
		
		$('.modal-backdrop').appendTo('#wrapper');
	});
	
	//get single cookie
	window.getCookie = function(name) {
	  match = document.cookie.match(new RegExp(name + '=([^;]+)'));
	  if (match) return match[1].replace('+', ' ');
	}
	
	//adding note in leads popup 
	$(document).on('click', '#btn-add-note', function() {
		var txtarea_notes = $('#txtarea-notes').val();
		if (txtarea_notes != '')  {
			var formattedDate = monthNames[date.getMonth()] +' '+ date.getDate() +', ' + date.getFullYear() +', ' + date.getHours() +':' + (date.getMinutes()<10?'0':'') + date.getMinutes() +' ' + period
			var txt_head = 'Note Added:';
			var txt_notes = $('#txtarea-notes').val();
			var random_num = Math.floor(Math.random() * 6);
			var logged_user = getCookie('login_user_name');
			
			$.ajax({
				type: "POST",
				url: "/createleadnotes",
				data: { 'created_id': getCookie('login_user_id'), 'created_by': logged_user, 
						'note_title': txt_head, 'note_body': txt_notes, 'created_at': date
					  }
			}).done(function(data) {
				var timeline_row = '<div id="'+ data['ret_id'] +'" class="row timeline-row" style="position:relative">' +
											'<div class="col-xs-1 col-md-1" style="height:60px;">' +
												'<div class="circle note-added"></div>' +
												'<div id="verticalline-'+ data['ret_id'] +'" class="verticalLine"></div>' +
											'</div>' +
											'<div class="col-xs-1 col-md-1 arrow-left"></div>' +
											'<div class="col-xs-10 col-md-9 timeline-container">' +
												'<ul class="list-inline">' +
													'<li class="timeline-head pull-left">' + txt_head + '</li>' +
													'<li class="pull-right"><i id="'+ data['ret_id'] +'" class="fa fa-times remove-timeline-row" style="position:relative; left:10px; top:-3px;" aria-hidden="true"></i></li>' +
												'</ul>' +
												'<p class="timeline-body" style="clear:both">' + txt_notes + '</p>' +
												'<ul class="list-inline timeline-foot">' +
													'<li class="col-xs-12 col-md-6 pull-left"><div class="dci-loading-div"></div></li>' +
													'<li class="col-xs-12 col-md-6 pull-right"><p class="timeline-datestamp" >'+ formattedDate +' By: '+ logged_user +'</p></li>' +
												'</ul>' +
											'</div>' +
										'</div>';
				var timeline_class = $(timeline_row).attr('class').split(' ').join('.');
				
				$('#dci-timeline')
					.prepend($(timeline_row)
					.animate({'top': '10'}, { duration: 1, 
					complete: function() {
						$('.timeline-container').css({ "background": "#e2e2e2" })
						$('.timeline-row').animate({top: 0}, {
							duration: 100});
					}})
				);
				
				//resize vertical line 
				if($('.' + timeline_class).height() !== 0) {
					$('#verticalline-' + data['ret_id']).height($('.' + timeline_class).height());
				} else {
					$('#verticalline-' + data['ret_id']).height(70);
					console.log('verticalline is not 0');
				}
				$('#txtarea-notes').val('');
			});
		} else {
			alert('Note is empty try again..');
		}
	})
	
	
	function showLeadTimeline() {
		$.ajax({
			type: "POST",
			url: "/getleadnotes",
			data: { 'login_user_id': getCookie('login_user_id') },
			async: true,
		}).done(function(data) {

			for(var i = 0; i < data['lead_notes'].length; i++) {
				var timeline_row = '<div id="'+ data['lead_notes'][i].id +'" class="row timeline-row" style="position:relative">' +
											'<div class="col-xs-1 col-md-1" style="height:60px;">' +
												'<div class="circle note-added"></div>' +
												'<div id="verticalline-'+ data['lead_notes'][i].id +'" class="verticalLine"></div>' +
											'</div>' +
											'<div class="col-xs-1 col-md-1 arrow-left"></div>' +
											'<div class="col-xs-10 col-md-9 timeline-container">' +
												'<ul class="list-inline">' +
													'<li class="timeline-head pull-left">' + data['lead_notes'][i].note_title + '</li>' +
													'<li class="pull-right"><i id="'+ data['lead_notes'][i].id +'" class="fa fa-times remove-timeline-row" style="position:relative; left:10px; top:-3px;" aria-hidden="true"></i></li>' +
												'</ul>' +
												'<p class="timeline-body" style="clear:both">' + data['lead_notes'][i].note_body + '</p>' +
												'<ul class="list-inline timeline-foot">' +
													'<li class="col-xs-12 col-md-6 pull-left"><div class="dci-loading-div"></div></li>' +
													'<li class="col-xs-12 col-md-6 pull-right"><p class="timeline-datestamp" >'+ data['lead_notes'][i].created_at +' By: '+ data['lead_notes'][i].created_by +'</p></li>' +
												'</ul>' +
											'</div>' +
										'</div>';
				
				
				$('#dci-timeline').prepend($(timeline_row)
							.animate({opacity: 0}, {
							duration: 0.5, 
							complete: function() {
								$('.timeline-container').css({ "background": "#e2e2e2" })
								$('.timeline-row').animate({opacity: 0.9}, {
									duration: 100});
							}})
						);
				var timeline_class = $(timeline_row).attr('class').split(' ').join('.');

				if($('.' + timeline_class).height() !== 0) {
					$('#verticalline-' + data['lead_notes'][i].id).height($('.' + timeline_class).height());
				} else {
					$('#verticalline-' + data['lead_notes'][i].id).height(80);
				}
			}
			
		});
		
		return false;
	}
	
	$(document).on('click', '.remove-timeline-row', function() {
		if(confirm("Are you sure you want to remove note?")){
        	$('#' + $(this).attr('id')).fadeOut(500);
			$.ajax({
				type: "POST",
				url: "/deleteleadnotes",
				data: { 'lead_note_id': $(this).attr('id') }
			}).done(function(data) {
				
			});
			
		}
		else{
			return false;
		}
		
	});
	
	
	
	
	
	
	
	