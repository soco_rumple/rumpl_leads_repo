<?php

use Phinx\Migration\AbstractMigration;

class Leads extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {	
		$table = $this->table('leads', array('primary_key' => array('id')));
		$table->addColumn('lead_name', 'string', array('limit' => 100))
				->addColumn('lead_company', 'string', array('limit' => 100))
				->addColumn('lead_title', 'string', array('limit' => 100))
				->addColumn('lead_email', 'string', array('limit' => 100))
				->addColumn('lead_phone', 'string', array('limit' => 100))
				->addColumn('lead_mobile', 'string', array('limit' => 100))
				->addColumn('lead_website', 'string', array('limit' => 100))
				->addColumn('lead_address', 'string', array('limit' => 255))
				->addColumn('table_id', 'integer', array('limit' => 12))
				->addColumn('created_at', 'datetime')
				->addColumn('updated_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
				->addIndex(array('created_at', 'lead_name'), array('name' => 'performance_index'))
				->create();
    }
}
