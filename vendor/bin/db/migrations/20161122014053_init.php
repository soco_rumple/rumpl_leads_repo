<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		$table = $this->table('tables', array('primary_key' => array('id')));
		$table->addColumn('table_name', 'string', array('limit' => 100))
				->addColumn('table_color', 'string', array('limit' => 12))
				->addColumn('table_ordinal_position', 'integer', array('limit' => 25))
				->addColumn('created_at', 'datetime')
				->addColumn('updated_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
				->addIndex(array('created_at'), array('name' => 'performance_index'))
				->create();

		
    }


}
