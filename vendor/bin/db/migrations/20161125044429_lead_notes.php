<?php

use Phinx\Migration\AbstractMigration;

class LeadNotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
		$table = $this->table('lead_notes', array('primary_key' => array('id')));
		$table->addColumn('note_title', 'string', array('limit' => 100))
				->addColumn('note_body', 'string', array('limit' => 1000))
				->addColumn('created_by', 'string', array('limit' => 25))
				->addColumn('created_user_id', 'integer', array('limit' => 12))
				->addColumn('created_at', 'datetime')
				->addColumn('updated_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
				->create();
    }
}
